package me.signatured.wildwestv2.listeners;

import java.util.Arrays;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DrugHarvest implements Listener {
	
	@EventHandler
	public void onDrugHarvest(BlockBreakEvent e) {
		Block block = e.getBlock();
		Player player = e.getPlayer();
		
		if (player.getGameMode() != GameMode.CREATIVE) {
			
			switch (block.getType()) {
			
			case RED_MUSHROOM:
				ItemStack shroom = new ItemStack(Material.RED_MUSHROOM);
				ItemMeta shroommeta = shroom.getItemMeta();
				String shroomname = "�eUnprocessed Mushrooms";
				
				shroommeta.setDisplayName(shroomname);
				shroommeta.setLore(Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
				shroom.setItemMeta(shroommeta);
				
				addItem(player, shroom, shroomname, block.getLocation());
				
				block.setType(Material.AIR);
				
				break;
				
			case SAPLING:
				ItemStack sapling = new ItemStack(Material.SAPLING);
				ItemMeta saplingmeta = sapling.getItemMeta();
				String saplingname = "�eUnprocessed Tobacco";
				
				saplingmeta.setDisplayName(saplingname);
				saplingmeta.setLore(Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
				sapling.setItemMeta(saplingmeta);
				
				addItem(player, sapling, saplingname, block.getLocation());
				
				block.setType(Material.AIR);
				
				break;
				
			case COCOA:
				ItemStack cocoa = new ItemStack(Material.INK_SACK, (byte)3);
				ItemMeta cocoameta = cocoa.getItemMeta();
				String cocoaname = "�eUnprocessed Coco";
				
				cocoameta.setDisplayName(cocoaname);
				cocoameta.setLore(Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
				cocoa.setItemMeta(cocoameta);
				
				addItem(player, cocoa, cocoaname, block.getLocation());
				
				block.setType(Material.AIR);
				
				break;
				
			case SUGAR_CANE_BLOCK:
				ItemStack sugar = new ItemStack(Material.SUGAR);
				ItemMeta sugarmeta = sugar.getItemMeta();
				String sugarname = "�eUnprocessed Mushrooms";
				
				sugarmeta.setDisplayName(sugarname);
				sugarmeta.setLore(Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
				sugar.setItemMeta(sugarmeta);
				
				addItem(player, sugar, sugarname, block.getLocation());
				
				block.setType(Material.AIR);
				
				break;
				
			case CROPS:
				ItemStack wheat = new ItemStack(Material.WHEAT);
				ItemMeta wehatmeta = wheat.getItemMeta();
				String wheatname = "�eUnprocessed Mushrooms";
				
				wehatmeta.setDisplayName(wheatname);
				wehatmeta.setLore(Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
				wheat.setItemMeta(wehatmeta);
				
				addItem(player, wheat, wheatname, block.getLocation());
				
				block.setType(Material.AIR);
				
				break;
				
			default:
				break;
			}
		}
	}
	
	public void addItem(Player player, ItemStack is, String drugname, Location loc) {
		if (player.getInventory().firstEmpty() != -1) {
			player.getInventory().addItem(is);
		} else {
			for (int i = 0; i < 35; i++) {
				ItemStack item = player.getInventory().getItem(i);
				
				if (item.getAmount() + 1 <= 64 && item.getType().equals(is.getType())) {
					if (item.hasItemMeta() && item.getItemMeta().getDisplayName().contains(drugname)) {
						player.getInventory().addItem(is);
						break;
					}
				}
				
				if (i == 34) {
					player.sendMessage("�cInventory full!");
					player.getWorld().dropItem(loc, is);
				}
			}
		}
	}
}
