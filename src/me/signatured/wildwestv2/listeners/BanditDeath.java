package me.signatured.wildwestv2.listeners;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.effects.Cross;
import me.signatured.wildwestv2.effects.DeathSkeleton;
import me.signatured.wildwestv2.spawnselectors.BanditSelector;
import me.signatured.wildwestv2.util.ClothingHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class BanditDeath implements Listener {
	
	private static BanditDeath instance;
	
	public BanditDeath() {
		instance = this;
	}
	
	public static BanditDeath getInstance() {
		return instance;
	}
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player killed = (Player) e.getEntity();
			Inventory inv = killed.getInventory();
			ItemStack[] gear = killed.getEquipment().getArmorContents();
			Location loc = killed.getLocation();
			
			if (e.getDamage() >= killed.getHealth()) {
				e.setCancelled(true);
				
				killed.setHealth(1D);
				killed.setFoodLevel(20);
				
				for (ItemStack is : inv.getContents()) {
					if (is != null && !killed.hasPermission("wildwest.sheriff")) {
						killed.getWorld().dropItem(loc, is);
					}
				}
				
				for (ItemStack is : gear) {
					if (is != null && is.getType() != Material.AIR && !killed.hasPermission("wildwest.sheriff")) {
						killed.getWorld().dropItem(loc, is);
					}
				}
				
				if (!killed.hasPermission("wildwest.sheriff")) {
					killed.playSound(killed.getLocation(), Sound.WITHER_SPAWN, 1, 1);
					
					TitleHandler.getInstance().sendTitleNoFade(killed, "�4YOU DIED!");
					Cross.getInstance().cross(killed);
					DeathSkeleton.getInstance().spawnSkeleton(killed, loc);
					
					killed.getInventory().clear();
					ClothingHandler.getInstance().clearArmor(killed);
					
					removePotionEffects(killed);
					killed.setWalkSpeed(0.2F);
					
					PlayerDataHandler.getInstance().set(killed, "dead", true);
					PlayerDataHandler.getInstance().set(killed, "handcuffed", false);
					PlayerDataHandler.getInstance().set(killed, "knocked out", false);
					
					countdown(killed);
					hidePlayer(killed);
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player killed = (Player) e.getEntity();
			Player killer = (Player) e.getDamager();
			Inventory inv = killed.getInventory();
			ItemStack[] gear = killed.getEquipment().getArmorContents();
			Location loc = killed.getLocation();
			
			if (e.getDamage() >= killed.getHealth()) {
				e.setCancelled(true);
				
				killed.setHealth(1D);
				killed.setFoodLevel(20);
				
				for (ItemStack is : inv.getContents()) {
					if (is != null && !killed.hasPermission("wildwest.sheriff")) {
						killed.getWorld().dropItem(loc, is);
					}
				}
				
				for (ItemStack is : gear) {
					if (is != null && !killed.hasPermission("wildwest.sheriff")) {
						killed.getWorld().dropItem(loc, is);
					}
				}
				
				if (!killed.hasPermission("wildwest.sheriff")) {
					killed.playSound(killed.getLocation(), Sound.WITHER_SPAWN, 1, 1);
					killer.playSound(killer.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
					
					TitleHandler.getInstance().sendTitleAndSubtitleNoFade(killed, "�4YOU DIED!", "�6Killed by �8" + killer.getName() + "�6!");
					TitleHandler.getInstance().sendSubtitleNoFade(killer, "�6Killed �8" + killed.getName() + "�6!");
					
					Cross.getInstance().cross(killed);
					DeathSkeleton.getInstance().spawnSkeleton(killed, killed.getLocation());
					
					killed.getInventory().clear();
					ClothingHandler.getInstance().clearArmor(killed);
					
					removePotionEffects(killed);
					killed.setWalkSpeed(0.2F);
					
					PlayerDataHandler.getInstance().set(killed, "knocked out", false);
					PlayerDataHandler.getInstance().set(killed, "handcuffed", false);
					PlayerDataHandler.getInstance().set(killed, "dead", true);
					
					countdown(killed);
					hidePlayer(killed);
				}
			}
		}
	}
	
	public void removePotionEffects(Player player) {
		player.removePotionEffect(PotionEffectType.JUMP);
		player.removePotionEffect(PotionEffectType.SLOW);
		player.removePotionEffect(PotionEffectType.BLINDNESS);
		player.removePotionEffect(PotionEffectType.INVISIBILITY);
		player.setWalkSpeed(0.2F);
	}
	
	public void countdown(Player player) {
		new BukkitRunnable() {
			int countdown = 10;
			
			@Override
			public void run() {
				if (!player.isOnline()) {
					this.cancel();
					return;
				}
				
				if (countdown == 0) {
					BountyPlayer bp = BountyPlayer.get(player);
					
					if (!player.hasPermission("wildwest.sheriff")) {
						if (bp.getBounty() > 0) {
							PlayerJoin.getInstance().checkBounty(player);
						} else {
							Location spawn = Bukkit.getWorld("world").getSpawnLocation();
							player.teleport(spawn);
							removePotionEffects(player);
							
							ClothingHandler.getInstance().giveDefaultGear(player);
							
							BanditSelector.getInstance().show(player);
							PlayerDataHandler.getInstance().set(player, "dead", false);
							PlayerDataHandler.getInstance().set(player, "spawnselector", true);
						}
					}
					this.cancel();
					return;
				}
				
				TitleHandler.getInstance().sendSubtitleNoFade(player, "�aRespawning in... �c" + countdown);
				countdown--;
			}
		}.runTaskTimer(me.signatured.wildwestv2.Main.getInstance(), 40, 20);
	}
	
	public void hidePlayer(final Player player) {
		 
        new BukkitRunnable() {
            
			@Override
            public void run() {
            	if (PlayerDataHandler.getInstance().get(player, "dead") == true) {
            		for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
    					pl.hidePlayer(player);
    				}
            	} else {
            		for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
    					pl.showPlayer(player);
    				}
            		this.cancel();
            		return;
            	}
            	
            }
        }.runTaskTimer(Main.getInstance(), 0, 1);
    }
}
