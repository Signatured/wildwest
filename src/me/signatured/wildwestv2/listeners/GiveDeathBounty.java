package me.signatured.wildwestv2.listeners;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.util.ActionBarHandler;

import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class GiveDeathBounty implements Listener {
	
	@EventHandler
	public void onPlayerDeath(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player damager = (Player) e.getDamager();
			Damageable damaged = (Damageable) e.getEntity();
			BountyPlayer bp = BountyPlayer.get(damager);
			
			if (e.getDamage() > damaged.getHealth()) {
				if (!damager.hasPermission("wildwest.sheriff")) {
					if (damaged.hasPermission("wildwest.sheriff")) {
						bp.setBounty(bp.getBounty() + 1500);
						ActionBarHandler.getInstance().sendActionbarMessage(damager, "�4�l+1500 Bounty");
					} else {
						bp.setBounty(bp.getBounty() + 1000);
						ActionBarHandler.getInstance().sendActionbarMessage(damager, "�4�l+1000 Bounty");
					}
				}
			}
			
			if (GiveDamageBounty.getInstance().tagged.containsKey(damaged)) GiveDamageBounty.getInstance().tagged.remove(damaged);
		}
	}
}
