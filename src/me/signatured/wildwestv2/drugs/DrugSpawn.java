package me.signatured.wildwestv2.drugs;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;

public class DrugSpawn {
	
	public static ArrayList<DrugSpawn> drugs = new ArrayList<DrugSpawn>();
	
	public int reset = 30;
	
	private Material type;
	private Location location;
	
	public DrugSpawn(Material type, Location loc) {
		this.type = type;
		this.location = loc;
		
		drugs.add(this);
	}
	
	public Location getLocation() {
		return location;
	}
	
	public Material getType() {
		return type;
	}
}
