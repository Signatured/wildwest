package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class HandcuffThrowItem implements Listener {
	
	@EventHandler
	public void onItemThrow(PlayerDropItemEvent e) {
		Player player = e.getPlayer();
		
		if (PlayerDataHandler.getInstance().get(player, "handcuffed") == true) {
			e.setCancelled(true);
			player.sendMessage(ChatHandler.getInstance().prefix + "�cCannot throw items while handcuffed!");
		}
	}
}
