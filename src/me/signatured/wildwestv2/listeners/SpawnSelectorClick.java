package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.spawnselectors.BanditSelector;
import me.signatured.wildwestv2.spawnselectors.SheriffSelector;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TownCreatorHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

public class SpawnSelectorClick implements Listener {
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		
		if (e.getInventory().getName().equalsIgnoreCase(BanditSelector.getInstance().inv.getName())) {
			if (!e.getInventory().getTitle().equals(InventoryType.CREATIVE)) {
				if (e.getCurrentItem() != null) {
					if (e.getCurrentItem().hasItemMeta()) {
						if (e.getCurrentItem().getItemMeta().getDisplayName() != null) {
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Town1")) {
								sendTown(e, player, ChatColor.GOLD + "You have respawned in town {town name}!", 0);
							}
						
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Town2")) {
								sendTown(e, player, ChatColor.GOLD + "You have respawned in town {town name}!", 1);
							}
							
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Town3")) {
								sendTown(e, player, ChatColor.GOLD + "You have respawned in town {town name}!", 2);
							}
							
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Town4")) {
								sendTown(e, player, ChatColor.GOLD + "You have respawned in town {town name}!", 3);
							}
							
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Private Home")) {
								e.setCancelled(true);
								
								PlayerDataHandler.getInstance().set(player, "spawnselector", false);
								player.closeInventory();
								Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "sendhome " + player.getName());
								if (player.hasPermission("wildwest.vip") || player.hasPermission("wildwest.mvp")) player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 0);
							}
							
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Gang HQ")) {
								e.setCancelled(true);
								
								PlayerDataHandler.getInstance().set(player, "spawnselector", false);
								player.closeInventory();
								Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "sendganghome " + player.getName());
								if (player.hasPermission("wildwest.vip") || player.hasPermission("wildwest.mvp")) player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 0);
							}
						}
					}
				}
			}
		}
		
		if (e.getInventory().getName().equalsIgnoreCase(SheriffSelector.getInstance().inv.getName())) {
			if (!e.getInventory().getTitle().equals(InventoryType.CREATIVE)) {
				if (e.getCurrentItem() != null) {
					if (e.getCurrentItem().hasItemMeta()) {
						if (e.getCurrentItem().getItemMeta().getDisplayName() != null) {
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("SheriffTown1")) {
								sendTown(e, player, ChatColor.GOLD + "You have respawned in town {town name}!", 0);
							}
						
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("SheriffTown2")) {
								sendTown(e, player, ChatColor.GOLD + "You have respawned in town {town name}!", 1);
							}
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("SheriffTown3")) {
								sendTown(e, player, ChatColor.GOLD + "You have respawned in town {town name}!", 2);
							}
							if (e.getCurrentItem().getItemMeta().getDisplayName().contains("SheriffTown4")) {
								sendTown(e, player, ChatColor.GOLD + "You have respawned in town {town name}!", 3);
							}
						}
					}
				}
			}
		}
	}
	
	public void sendTown(Event e, Player player, String respawn, int number) {
		((InventoryClickEvent) e).setCancelled(true);
		PlayerDataHandler.getInstance().set(player, "spawnselector", false);
		player.closeInventory();
		player.sendMessage(ChatHandler.getInstance().prefix + respawn);
		player.teleport(TownCreatorHandler.getInstance().towns.get(number).getTownLocation());
		player.getLocation().setPitch((float) TownCreatorHandler.getInstance().towns.get(number).getTownPitch());
		player.getLocation().setYaw((float) TownCreatorHandler.getInstance().towns.get(number).getTownYaw());
		if (player.hasPermission("wildwest.vip") || player.hasPermission("wildwest.mvp") || player.hasPermission("sheriff.issheriff")) player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 0);
	}
}
