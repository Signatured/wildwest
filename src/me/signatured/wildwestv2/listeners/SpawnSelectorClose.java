package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.spawnselectors.BanditSelector;
import me.signatured.wildwestv2.spawnselectors.SheriffSelector;
import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class SpawnSelectorClose implements Listener {
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		final Player player = (Player) e.getPlayer();
	
		if (e.getInventory().getName().equalsIgnoreCase(BanditSelector.getInstance().inv.getName())) {
			Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				@Override
				public void run() {
					if (PlayerDataHandler.getInstance().get(player, "spawnselector") == true) {
						BanditSelector.getInstance().show(player);
					}
				}
			}, 2);
		}
		
		if (player.hasPermission("wildwest.sheriff")) {
			if (e.getInventory().getName().equalsIgnoreCase(SheriffSelector.getInstance().inv.getName())) {
				Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
					@Override
					public void run() {
						if (PlayerDataHandler.getInstance().get(player, "spawnselector") == true) {
							SheriffSelector.getInstance().show(player);
						}
					}
				}, 2);
			}
		}
	}
}
