package me.signatured.wildwestv2.effects;

import java.util.HashMap;

import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.util.HoloHandler;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

public class Cross {
	
	public HashMap<Player, Location> crosses = new HashMap<Player, Location>();
	
	private static Cross instance;
	
	public Cross() {
		instance = this;
	}
	
	public static Cross getInstance() {
		return instance;
	}
	
	public void cross(Player player) {
		Location loc = player.getLocation();
		Block block = loc.getBlock();
		
		if (block == null || block.isEmpty() || block.getType() == Material.AIR) {
			while (true) {
				Block down = block.getRelative(BlockFace.DOWN);
				
				if (block == null || block.isEmpty() || block.getType() == Material.AIR)
					block = down;
				else if (down.getY() <= 2)
					return;
				else {
					block = block.getRelative(BlockFace.UP);
					break;
				}
			}
		}
		
		double x = block.getRelative(BlockFace.UP).getLocation().getX() + 0.5;
		double y = block.getRelative(BlockFace.UP).getLocation().getY() + 0.5;
		double z = block.getRelative(BlockFace.UP).getLocation().getZ() + 0.5;
		World world = block.getRelative(BlockFace.UP).getLocation().getWorld();
		Location hololoc = new Location(world, x, y, z);
		
		if (block.getType() == Material.AIR || block == null || block.isEmpty()) {
			HoloHandler.getInstance().createCrossHolo(hololoc, "�c�lRIP �8�l" + player.getName());
			block.setType(Material.CARPET);
			crosses.put(player, block.getLocation());
			
			Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				@Override
				public void run() {
					Location crossloc = crosses.get(player);
					crossloc.getBlock().setType(Material.AIR);
				}
			}, 20 * 30);
		}
	}
}
