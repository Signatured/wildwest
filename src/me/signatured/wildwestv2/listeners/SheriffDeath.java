package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.effects.Cross;
import me.signatured.wildwestv2.effects.DeathSkeleton;
import me.signatured.wildwestv2.spawnselectors.SheriffSelector;
import me.signatured.wildwestv2.util.ClothingHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class SheriffDeath implements Listener {
	
	public PlayerDataHandler playerdata = PlayerDataHandler.getInstance();
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player killed = (Player) e.getEntity();
			Inventory inv = killed.getInventory();
			Location loc = killed.getLocation();
			
			if (e.getDamage() >= killed.getHealth()) {
				e.setCancelled(true);
				
				killed.setHealth(1D);
				killed.setFoodLevel(20);
				
				for (ItemStack is : inv.getContents()) {
					if (is != null && is.getType() != Material.AIR && killed.hasPermission("wildwest.sheriff")) {
						is.setType(Material.AIR);
					}
				}
				
				if (killed.hasPermission("wildwest.sheriff")) {
					ItemStack skull = new ItemStack(397, 1, (byte)3);
					SkullMeta meta = (SkullMeta) skull.getItemMeta();
					meta.setOwner(killed.getName());
					meta.setDisplayName("�6Sheriff �8" + killed.getName() + "'s �6head");
					skull.setItemMeta(meta);
					
					loc.getWorld().dropItem(loc, skull);
					killed.getInventory().clear();
					ClothingHandler.getInstance().clearArmor(killed);
					
					Cross.getInstance().cross(killed);
					DeathSkeleton.getInstance().spawnSkeleton(killed, killed.getLocation());
					killed.playSound(killed.getLocation(), Sound.WITHER_SPAWN, 1, 5);
					
					TitleHandler.getInstance().sendTitleNoFade(killed, "�4YOU DIED!");
					
					killed.getInventory().clear();
					ClothingHandler.getInstance().clearArmor(killed);
					removePotionEffects(killed);
					
					hidePlayer(killed);
					
					playerdata.set(killed, "dead", true);
					playerdata.set(killed, "handcuffed", false);
					playerdata.set(killed, "knocked out", false);
					
					countdown(killed);
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player killed = (Player) e.getEntity();
			Player killer = (Player) e.getDamager();
			Inventory inv = killed.getInventory();
			Location loc = killed.getLocation();
			
			if (e.getDamage() >= killed.getHealth()) {
				e.setCancelled(true);
				
				killed.setHealth(1D);
				killed.setFoodLevel(20);
				
				for (ItemStack is : inv.getContents()) {
					if (is != null && killed.hasPermission("wildwest.sheriff")) {
						is.setType(Material.AIR);
					}
				}
				
				if (killed.hasPermission("wildwest.sheriff")) {
					ItemStack skull = new ItemStack(397, 1, (byte)3);
					SkullMeta meta = (SkullMeta) skull.getItemMeta();
					meta.setOwner(killed.getName());
					meta.setDisplayName("�6Sheriff �8" + killed.getName() + "'s �6head");
					skull.setItemMeta(meta);
					
					loc.getWorld().dropItem(loc, skull);
					killed.getInventory().clear();
					ClothingHandler.getInstance().clearArmor(killed);
					
					Cross.getInstance().cross(killed);
					DeathSkeleton.getInstance().spawnSkeleton(killed, killed.getLocation());
					killed.playSound(killed.getLocation(), Sound.WITHER_SPAWN, 1, 5);
					killed.playSound(killer.getLocation(), Sound.SUCCESSFUL_HIT, 1, 5);
					
					TitleHandler.getInstance().sendTitleAndSubtitleNoFade(killed, "�4YOU DIED!", "�6Killed by �8" + killer.getName());
					TitleHandler.getInstance().sendSubtitleNoFade(killer, "�6Killed �8" + killed.getName());
					
					killed.getInventory().clear();
					ClothingHandler.getInstance().clearArmor(killed);
					removePotionEffects(killed);
					killed.setWalkSpeed(0.2F);
					
					killed.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000, 1000));
					hidePlayer(killed);
					
					playerdata.set(killed, "dead", true);
					playerdata.set(killed, "handcuffed", false);
					playerdata.set(killed, "knocked out", false);
					
					countdown(killed);
				}
			}
		}
	}
	
	public void removePotionEffects(Player player) {
		player.removePotionEffect(PotionEffectType.JUMP);
		player.removePotionEffect(PotionEffectType.SLOW);
		player.removePotionEffect(PotionEffectType.BLINDNESS);
		player.removePotionEffect(PotionEffectType.INVISIBILITY);
		player.setWalkSpeed(0.2F);
	}
	
	public void countdown(Player player) {
		new BukkitRunnable() {
			int countdown = 10;
			
			@Override
			public void run() {
				if (!player.isOnline()) {
					this.cancel();
					return;
				}
				
				if (countdown == 0) {
					if (player.hasPermission("wildwest.sheriff")) {
						Location spawn = Bukkit.getWorld("world").getSpawnLocation();
						player.teleport(spawn);
						giveSheriffEquiptment(player);
						removePotionEffects(player);
						
						SheriffSelector.getInstance().show(player);
						PlayerDataHandler.getInstance().set(player, "dead", false);
						PlayerDataHandler.getInstance().set(player, "spawnselector", true);
					}
					this.cancel();
					return;
				}
				TitleHandler.getInstance().sendSubtitleNoFade(player, "�aRespawning in... �c" + countdown);
				countdown--;
			}
		}.runTaskTimer(Main.getInstance(), 40, 20);
	}
	
	public void giveSheriffEquiptment(Player player) {
		Inventory inv = player.getInventory();
		
		ItemStack handcuffs = new ItemStack(Material.BLAZE_ROD);
		ItemMeta handcuffsmeta = handcuffs.getItemMeta();
		handcuffsmeta.setDisplayName(ChatColor.RED + "Handcuffs");
		handcuffs.setItemMeta(handcuffsmeta);
		
		ItemStack sheriffstick = new ItemStack(Material.STICK);
		ItemMeta sheriffmeta = sheriffstick.getItemMeta();
		sheriffmeta.setDisplayName(ChatColor.BLUE + "Inspector");
		sheriffstick.setItemMeta(sheriffmeta);
		
		ItemStack ticket = new ItemStack(Material.PAPER);
		ItemMeta ticketmeta = ticket.getItemMeta();
		ticketmeta.setDisplayName(ChatColor.YELLOW + "Ticket");
		ticket.setItemMeta(ticketmeta);
		
		inv.setItem(6, handcuffs);
		inv.setItem(7, sheriffstick);
		inv.setItem(8, ticket);
	}
	
	public void hidePlayer(final Player player) {
		 
        new BukkitRunnable() {
            
			@Override
            public void run() {
            	if (PlayerDataHandler.getInstance().get(player, "dead") == true) {
            		for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
    					pl.hidePlayer(player);
    				}
            	} else {
            		for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
    					pl.showPlayer(player);
    				}
            		this.cancel();
            		return;
            	}
            	
            }
        }.runTaskTimer(Main.getInstance(), 0, 1);
    }
}
