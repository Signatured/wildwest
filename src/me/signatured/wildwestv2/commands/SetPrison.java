package me.signatured.wildwestv2.commands;

import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.ConfigHandler;

import org.bukkit.entity.Player;

public class SetPrison extends WildWestCommand {
	
	@Override
	public String name() {
		return "setprison";
	}
	
	@Override
	public String description() {
		return "Set prison spawn";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			if (args.length > 0) {
				sender.sendMessage("�c/setprison");
				return;
			}
			
			float pitch = sender.getEyeLocation().getPitch();
			float yaw = sender.getEyeLocation().getYaw();
			ConfigHandler.getInstance().getConfig().set("prison.world", sender.getLocation().getWorld().getName());
			ConfigHandler.getInstance().getConfig().set("prison.x", sender.getLocation().getX());
			ConfigHandler.getInstance().getConfig().set("prison.y", sender.getLocation().getY());
			ConfigHandler.getInstance().getConfig().set("prison.z", sender.getLocation().getZ());
			ConfigHandler.getInstance().getConfig().set("prison.pitch", pitch);
			ConfigHandler.getInstance().getConfig().set("prison.yaw", yaw);
			ConfigHandler.getInstance().save();
			sender.sendMessage("�aPrison spawn set!");
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
