package me.signatured.wildwestv2.listeners;

import java.util.Random;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.spawnselectors.BanditSelector;
import me.signatured.wildwestv2.util.ActionBarHandler;
import me.signatured.wildwestv2.util.ClothingHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.Note.Tone;
import org.bukkit.block.Block;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class PrisonOreMine implements Listener {
	
	@EventHandler
	public void onPrisonMine(BlockBreakEvent e) {
		Player player = e.getPlayer();
		Block block = e.getBlock();
		BountyPlayer bp = BountyPlayer.get(player);
		
		Random random = new Random();
		int x = random.nextInt(2);
		String bountyreduction = null;
		
		if (PlayerDataHandler.getInstance().get(player, "prison") == true) {
			if (bp.getBounty() != 0) {
				
				switch (block.getType()) {
				
				case COAL_ORE:
					if (x == 0) bountyreduction = "�2�l-10 Bounty";
					if (x == 1) bountyreduction = "�a�l-10 Bounty";
					
					oreMined(block, bountyreduction, player, bp.getBounty(), 10);
					
					if (bp.getBounty() < 0) bp.setBounty(0);
					if (bp.getBounty() == 0) paidBounty(player);
					break;
					
				case IRON_ORE:
					if (x == 0) bountyreduction = "�2�l-25 Bounty";
					if (x == 1) bountyreduction = "�a�l-25 Bounty";
					
					oreMined(block, bountyreduction, player, bp.getBounty(), 25);
					
					if (bp.getBounty() < 0) bp.setBounty(0);
					if (bp.getBounty() == 0) paidBounty(player);
					break;
					
				case GOLD_ORE:
					if (x == 0) bountyreduction = "�2�l-50 Bounty";
					if (x == 1) bountyreduction = "�a�l-50 Bounty";
					
					oreMined(block, bountyreduction, player, bp.getBounty(), 50);
					
					if (bp.getBounty() < 0) bp.setBounty(0);
					if (bp.getBounty() == 0) paidBounty(player);
					break;
					
				case DIAMOND_ORE:
					if (x == 0) bountyreduction = "�2�l-100 Bounty";
					if (x == 1) bountyreduction = "�a�l-100 Bounty";
					
					oreMined(block, bountyreduction, player, bp.getBounty(), 100);
					
					if (bp.getBounty() < 0) bp.setBounty(0);
					if (bp.getBounty() == 0) paidBounty(player);
					break;
					
				case EMERALD_ORE:
					if (x == 0) bountyreduction = "�2�l-200 Bounty";
					if (x == 1) bountyreduction = "�a�l-200 Bounty";
					
					oreMined(block, bountyreduction, player, bp.getBounty(), 200);
					
					if (bp.getBounty() < 0) bp.setBounty(0);
					if (bp.getBounty() == 0) paidBounty(player);
					break;
					
				default:
					break;
				}
			}
		}
	}
	
	public static void shootfirework(Player p) {
		for(int i = 0; i < 5; i++) {
			Location loc = p.getLocation();
			Firework fw = p.getWorld().spawn(loc.add(0.5, 0, -0.5),
					Firework.class);
			FireworkMeta fm = fw.getFireworkMeta();

			Random r = new Random();

			Color c1 = Color
					.fromBGR(r.nextInt(255), r.nextInt(255), r.nextInt(255));
			Color c2 = Color
					.fromBGR(r.nextInt(255), r.nextInt(255), r.nextInt(255));
			
			FireworkEffect eff = FireworkEffect.builder().flicker(r.nextBoolean())
					.withColor(c1).withFade(c2).with(FireworkEffect.Type.BALL).trail(r.nextBoolean())
					.build();
			
			int power = r.nextInt(2) + 1;
			fm.setPower(power);
			fm.addEffect(eff);
			fw.setFireworkMeta(fm);
			fw.detonate();
		}
	}
	
	public static void paidBounty(final Player player) {
		player.getInventory().clear();
		countdown(player);
		shootfirework(player);
		Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			@Override
			public void run() {
				PlayerDataHandler.getInstance().set(player, "spawnselector", true);
				PlayerDataHandler.getInstance().set(player, "prison", false);
				
				BanditSelector.getInstance().show(player);
				
				ClothingHandler.getInstance().clearArmor(player);
				ClothingHandler.getInstance().giveDefaultGear(player);
				
				player.playSound(player.getLocation(), Sound.LEVEL_UP, 60, 0);
			}
		}, 100);
	}
	
	public static void oreMined(Block block, String string, Player player, int bounty, int bountyremoved) {
		BountyPlayer bp = BountyPlayer.get(player);
		
		block.setType(Material.AIR);
		bp.setBounty(bounty - bountyremoved);
		ActionBarHandler.getInstance().sendActionbarMessage(player, string);
	}
	
	public static void countdown(final Player player) {
        new BukkitRunnable() {
            int countdown = 5;
            @Override
            public void run(){
            	if (!player.isOnline()) {
            		this.cancel();
            		return;
            	}
            	
                if (countdown <= 0) {
                	TitleHandler.getInstance().clearTitleAndSubtitle(player);
                	PlayerDataHandler.getInstance().set(player, "prison", false);
                	
                    this.cancel();
                    return;
                }
                if (countdown == 3 || countdown == 2 || countdown == 1) {
                	player.playNote(player.getLocation(), Instrument.PIANO, Note.natural(1, Tone.C));
                }
                
                TitleHandler.getInstance().sendTitleAndSubtitleNoFade(player, "�6Bounty Paid!", "�8Choose a town in... �a" + countdown);
                countdown--;
 
            }
        }.runTaskTimer(Main.getInstance(), 0, 20);

    }
}
