package me.signatured.wildwestv2.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

public class JailUtil {
	
	private static JailUtil instance;
	
	public JailUtil() {
		instance = this;
	}
	
	public static JailUtil getInstance() {
		return instance;
	}
	
	public void sendToJail(Player player, Inventory inv) {
		player.setWalkSpeed(0.2F);
		player.removePotionEffect(PotionEffectType.JUMP);
		player.removePotionEffect(PotionEffectType.BLINDNESS);
		player.removePotionEffect(PotionEffectType.JUMP);
		
		inv.clear();
		ClothingHandler.getInstance().clearArmor(player);
		ClothingHandler.getInstance().giveJailArmor(player);
		
		player.closeInventory();
		PlayerDataHandler.getInstance().set(player, "prison", true);
	
		World w = Bukkit.getWorld(ConfigHandler.getInstance().getConfig().getString("prison.world"));
		double x = ConfigHandler.getInstance().getConfig().getDouble("prison.x");
		double y = ConfigHandler.getInstance().getConfig().getDouble("prison.y");
		double z = ConfigHandler.getInstance().getConfig().getDouble("prison.z");
		float pitch = (float) ConfigHandler.getInstance().getConfig().getDouble("prison.pitch");
		float yaw = (float) ConfigHandler.getInstance().getConfig().getDouble("prison.yaw");
	
		ItemStack pick = new ItemStack(Material.DIAMOND_PICKAXE, 1);
		ItemMeta pickmeta = pick.getItemMeta();
		
		Location loc = new Location (w, x, y, z);
		loc.setYaw(yaw);
		player.getLocation().setPitch(pitch);
		player.teleport(loc);
	
		pickmeta.setDisplayName(ChatColor.AQUA + "Diamond Pickaxe");
		pick.setItemMeta(pickmeta);
		player.getInventory().setItem(0, pick);
		
	}
}
