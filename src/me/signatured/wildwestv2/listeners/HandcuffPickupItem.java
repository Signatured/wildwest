package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class HandcuffPickupItem implements Listener {

	@EventHandler
	public void onItemPickup(PlayerPickupItemEvent e) {
		Player player = e.getPlayer();
		
		if (PlayerDataHandler.getInstance().get(player, "handcuffed") == true) {
			e.setCancelled(true);
		}
	}
}
