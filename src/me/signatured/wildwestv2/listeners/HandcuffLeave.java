package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.effects.Cross;
import me.signatured.wildwestv2.effects.DeathSkeleton;
import me.signatured.wildwestv2.util.ClothingHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class HandcuffLeave implements Listener {
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		
		if (PlayerDataHandler.getInstance().get(player, "handcuffed") == true) {
			Cross.getInstance().cross(player);
			DeathSkeleton.getInstance().spawnSkeleton(player, player.getLocation());
			for (ItemStack items : player.getInventory().getContents()) {
				if (!(items == null)) {
					player.getLocation().getWorld().dropItem(player.getLocation(), items);
				}
			}
			ClothingHandler.getInstance().clearArmor(player);
			player.getInventory().clear();
			PlayerDataHandler.getInstance().set(player, "dead", true);
		}
	}
}
