package me.signatured.wildwestv2.commands;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;

import org.bukkit.entity.Player;

public class Test extends WildWestCommand {
	
	@Override
	public String name() {
		return "test";
	}
	
	@Override
	public String description() {
		return "Test bounty plugin";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			BountyPlayer bp = BountyPlayer.get(sender);
			bp.setBounty(bp.getBounty() + 1);
			sender.sendMessage("�aDone");
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
