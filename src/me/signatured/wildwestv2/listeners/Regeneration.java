package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;

public class Regeneration implements Listener {
	
	@EventHandler
    public void onPlayerRegainHealth(EntityRegainHealthEvent e) {
        if (e.getEntity() instanceof Player) {
        	Player player = (Player) e.getEntity();
        	
        	if (PlayerDataHandler.getInstance().get(player, "knocked out") == true) {
        		e.setCancelled(true);
        	}
        }
    }
}
