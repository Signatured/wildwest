package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.drugs.DrugSpawn;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class DrugPlant implements Listener {
	
	@EventHandler
	public void onDrugPlace(BlockPlaceEvent e) {
		Block block = e.getBlock();
		Player player = e.getPlayer();
		
		if (player.hasPermission("wildwest.admin")) {
			switch (block.getType()) {
			
			case RED_MUSHROOM:
				new DrugSpawn(block.getType(), block.getLocation());
				break;
			
			case COCOA:
				new DrugSpawn(block.getType(), block.getLocation());
				break;
				
			case CROPS:
				new DrugSpawn(block.getType(), block.getLocation());
				break;
				
			case SAPLING:
				new DrugSpawn(block.getType(), block.getLocation());
				break;
				
			case SUGAR_CANE_BLOCK:
				new DrugSpawn(block.getType(), block.getLocation());
				break;
				
			default:
				break;
			}
		}
	}
}
