package me.signatured.wildwestv2.listeners;

import java.util.ArrayList;
import java.util.List;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Chat implements Listener {
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		
		String global = "[Global] ";
		String local = "[Local] ";
		
		if (PlayerDataHandler.getInstance().getString(player, "chat").equals("local")) {
			List<Entity> nbe = new ArrayList<Entity>();
			nbe = player.getNearbyEntities(15, 15, 15);
			
			ArrayList<Player> nearby = new ArrayList<Player>();
			nearby.add(player);
			
			e.setFormat(local + "%s: %s");
			
			for (Entity entity : nbe) {
				if (entity instanceof Player) {
					Player p = (Player) entity;
					nearby.add(p);
				}
			}
			
			for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
				if (!nearby.contains(pl)) {
					e.getRecipients().remove(pl);
				}
			}
			
		} else if (PlayerDataHandler.getInstance().getString(player, "chat").equals("global")) {
			System.out.println("player chatting in global");
			
			for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
				if (!(PlayerDataHandler.getInstance().getString(player, "chat").equals("global"))) {
					pl.sendMessage("removed LOLOLOL");
					e.getRecipients().remove(pl);
				}
			}
			e.setFormat(global + "%s: %s");
		}
	}
}
