package me.signatured.wildwestv2.util;

import io.puharesource.mc.titlemanager.api.ActionbarTitleObject;

import org.bukkit.entity.Player;

public class ActionBarHandler {
	
	private static ActionBarHandler instance;
	
	public ActionBarHandler() {
		instance = this;
	}
	
	public static ActionBarHandler getInstance() {
		return instance;
	}
	
	public void sendActionbarMessage(Player player, String message) {
		new ActionbarTitleObject(message).send(player);
	}
	
	public void sendActionbarBroadcast(Player player, String message) {
		new ActionbarTitleObject(message).broadcast();
	}
	
	public void clearActionbar(Player player) {
		new ActionbarTitleObject(" ").send(player);
	}
}
