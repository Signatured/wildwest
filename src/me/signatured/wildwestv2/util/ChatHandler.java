package me.signatured.wildwestv2.util;

import org.bukkit.ChatColor;

public class ChatHandler {
	
	private static ChatHandler instance;
	
	public ChatHandler() {
		instance = this;
	}
	
	public static ChatHandler getInstance() {
		return instance;
	}
	
	public String prefix = ChatColor.translateAlternateColorCodes('&', "&8[&6WildWest&8] ");
}
