package me.signatured.wildwestv2.listeners;

import java.util.ArrayList;
import java.util.List;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

public class TicketUse implements Listener {
	
	private static TicketUse instance;
	
	public TicketUse() {
		instance = this;
	}
	
	public static TicketUse getInstance() {
		return instance;
	}
	
	public String suffix = " shillings ticket!";
	
	public Inventory inv;
	private ItemStack pay, jail;
	
	public TicketUse(Player player) {
		
		BountyPlayer bp = BountyPlayer.get(player);
		String invname = "�c" + bp.getBounty() + suffix;
		
		inv = Bukkit.getServer().createInventory(null, 9, invname.substring(0, Math.min(32, invname.length())));
		
		pay = createItem(DyeColor.GREEN, "�aPay Bounty", "�5Pay off bounty", "�5from bank!");
		jail = createItem(DyeColor.RED, "�aGo to Jail", "�5Pay off bounty", "�5in jail!");
		
		inv.setItem(2, pay);
		inv.setItem(6, jail);
		
	}
	
	@EventHandler
	public void onTicketUse(PlayerInteractEntityEvent e) {
		Player player = e.getPlayer();
		
		if (e.getRightClicked() instanceof Player) {
			Player clicked = (Player) e.getRightClicked();
			
			if (player.getItemInHand().getType() == Material.STICK &&
					player.getItemInHand().getItemMeta().getDisplayName().equals("�eTicket")) {
				if (PlayerDataHandler.getInstance().get(clicked, "ticket") == false) {
					if (PlayerDataHandler.getInstance().get(clicked, "handcuffed") == true) {
						TitleHandler.getInstance().sendSubtitleNoFade(player, "�6Gave �8" + clicked.getName() + " �6 a ticket");
						show(clicked);
					} else {
						player.sendMessage(ChatHandler.getInstance().prefix + "�8" + clicked.getName() + "�c must be in handcuffs to be given a ticket!");
					}
				} else {
					PlayerDataHandler.getInstance().set(clicked, "ticket", false);
					clicked.closeInventory();
					TitleHandler.getInstance().sendSubtitleNoFade(player, "�6Removed ticket from �8" + clicked.getName());
					TitleHandler.getInstance().sendSubtitleNoFade(clicked, "�6Ticket Removed");
				}
			}
		}
	}
	
	private ItemStack createItem(DyeColor dc, String name, String lore1, String lore2) {
		ItemStack i = new Wool(dc).toItemStack(1);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		List<String> lore = new ArrayList<String>();
		lore.add(lore1);
		lore.add(lore2);
		im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}
	
	public void show(Player player) {
		player.openInventory(inv);
		PlayerDataHandler.getInstance().set(player, "ticket", true);
	}
	
	public Inventory getTicketInv() {
		return inv;
	}
}
