package me.signatured.wildwestv2;

import java.util.ArrayList;

import me.signatured.wildwestv2.commands.Channel;
import me.signatured.wildwestv2.commands.ClearBounty;
import me.signatured.wildwestv2.commands.GiveHandcuffs;
import me.signatured.wildwestv2.commands.GiveInspector;
import me.signatured.wildwestv2.commands.GiveTicket;
import me.signatured.wildwestv2.commands.Handcuff;
import me.signatured.wildwestv2.commands.SaveTowns;
import me.signatured.wildwestv2.commands.SetBounty;
import me.signatured.wildwestv2.commands.SetPrison;
import me.signatured.wildwestv2.commands.SetSheriffTown;
import me.signatured.wildwestv2.commands.SetTown;
import me.signatured.wildwestv2.commands.Test;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandManager implements CommandExecutor {
	
	public ArrayList<WildWestCommand> commands = new ArrayList<WildWestCommand>();
	public ArrayList<WildWestCommand> admincommands = new ArrayList<WildWestCommand>();
	
	public CommandManager() {
		admincommands.add(new ClearBounty());
		admincommands.add(new GiveHandcuffs());
		admincommands.add(new GiveInspector());
		admincommands.add(new GiveTicket());
		admincommands.add(new Handcuff());
		admincommands.add(new SetBounty());
		admincommands.add(new SetPrison());
		admincommands.add(new SetSheriffTown());
		admincommands.add(new SetTown());
		admincommands.add(new Test());
		admincommands.add(new SaveTowns());		
		commands.add(new Channel());
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("wildwest.admin") || player.isOp()) {
				if (args.length == 0) {
					for (WildWestCommand command : admincommands) {
						player.sendMessage("�a/wildwest " + command.name() + "�d - " + command.description());
					}
					for (WildWestCommand command : commands) {
						player.sendMessage("�a/wildwest " + command.name() + "�d - " + command.description());
					}
					return true;
				}
				
				WildWestCommand found = null;
				
				for (WildWestCommand command : commands) {
					if (args[0].equalsIgnoreCase(command.name())) {
						found = command;
						break;
					}
				}
				
				if (found == null) {
					player.sendMessage("�cError: Command not found. Type /wildwest for a list of commands.");
					return true;
				}
				
				ArrayList<String> argslist = new ArrayList<String>();
				
				for (String s : args) {
					argslist.add(s);
				}
				
				argslist.remove(0);
				
				String[] newargs = argslist.toArray(new String[argslist.size()]);
				
				found.run(newargs, player);
			} else {
				if (args.length == 0) {
					for (WildWestCommand command : commands) {
						player.sendMessage("�a/wildwest " + command.name() + "�d - " + command.description());
					}
					return true;
				}
				
				WildWestCommand found = null;
				
				for (WildWestCommand command : commands) {
					if (args[0].equalsIgnoreCase(command.name())) {
						found = command;
						break;
					}
				}
				
				if (found == null) {
					player.sendMessage("�cError: Command not found. Type /wildwest for a list of commands.");
					return true;
				}
				
				ArrayList<String> argslist = new ArrayList<String>();
				
				for (String s : args) {
					argslist.add(s);
				}
				
				argslist.remove(0);
				
				String[] newargs = argslist.toArray(new String[argslist.size()]);
				
				found.run(newargs, player);
			}
		}
		return true;
	}
}
