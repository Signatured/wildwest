package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.util.ActionBarHandler;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class HandcuffUse implements Listener {
	
	public PlayerDataHandler playerdata = PlayerDataHandler.getInstance();
	
	@EventHandler
	public void onHandcuffUse(PlayerInteractEntityEvent e) {
		Player player = e.getPlayer();
		
		if (e.getRightClicked() instanceof Player) {
			Player clicked = (Player) e.getRightClicked();
			
			if (player.getItemInHand().getType() == Material.BLAZE_ROD &&
					player.getItemInHand().getItemMeta().getDisplayName().equals("&cHandcuffs")) {
				if (!clicked.hasPermission("wildwest.sheriff")) {
					if (playerdata.get(clicked, "handcuffed") == false) {
						playerdata.set(clicked, "handcuffed", false);
						
						TitleHandler.getInstance().sendSubtitleNoFade(player, "�6Handcuffed �8" + clicked.getName());
						TitleHandler.getInstance().sendSubtitleNoFade(clicked, "�6Handcuffed by Sheriff �8" + player.getName());
						
						clicked.setWalkSpeed(0F);
						clicked.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 100000, 250));
						
						countdown(clicked);
					} else {
						if (playerdata.get(clicked, "knockedout") == true) {
							playerdata.set(clicked, "handcuffed", false);
							
							TitleHandler.getInstance().sendSubtitleNoFade(player, "�6Unhandcuffed �8" + clicked.getName());
							TitleHandler.getInstance().sendSubtitleNoFade(clicked, "�6Unhandcuffed by Sheriff �8" + player.getName());
						}
						playerdata.set(clicked, "handcuffed", true);
						
						TitleHandler.getInstance().sendSubtitleNoFade(player, "�6Unhandcuffed " + clicked.getName());
						TitleHandler.getInstance().sendSubtitleNoFade(clicked, "�6Unhandcuffed by Sheriff " + player.getName());
						
						clicked.setWalkSpeed(0.2F);
						clicked.removePotionEffect(PotionEffectType.JUMP);
					}
				} else {
					player.sendMessage(ChatHandler.getInstance().prefix + "�cYou can't handcuff other sheriffs!");
				}
			}
		}
	}
	
	public void countdown(Player player) {
		new BukkitRunnable() {
			int countdown = 600;
			@Override
			public void run(){
				if (!player.isOnline()) {
					this.cancel();
					return;
				}
				
				if (PlayerDataHandler.getInstance().get(player, "handcuffed") == false) {
					ActionBarHandler.getInstance().clearActionbar(player);
					this.cancel();
					return;
				}
            	
				if (countdown <= 0) {
					TitleHandler.getInstance().sendSubtitleNoFade(player, "�6Automatically unhandcuffed");
					PlayerDataHandler.getInstance().set(player, "handcuffed", false);
					this.cancel();
					return;
				}
 
				ActionBarHandler.getInstance().sendActionbarMessage(player, "�c�lHANDCUFFED (" + countdown + ")");
				countdown--;
 
			}
		}.runTaskTimer(Main.getInstance(), 0, 20);
	}
}
