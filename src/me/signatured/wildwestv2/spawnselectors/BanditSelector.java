package me.signatured.wildwestv2.spawnselectors;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

public class BanditSelector {
	
	private static BanditSelector instance;
	
	public static BanditSelector getInstance() {
		return instance;
	}
	
	public Inventory inv;
	
	private ItemStack town1, town2, town3, town4;
	private ItemStack home, ganghq;
	
	public BanditSelector() {
		instance = this;
		inv = Bukkit.getServer().createInventory(null, 18, "Choose a spawn location!");
		
		town1 = createTown(DyeColor.GREEN, "�bTown1");
		town2 = createTown(DyeColor.YELLOW, "�bTown2");
		town3 = createTown(DyeColor.BLUE, "�bTown3");
		town4 = createTown(DyeColor.RED, "�bTown4");
		
		home = createHome(new ItemStack(Material.WOOD_DOOR, 1), "�6Private Home", Arrays.asList("�5Must own a", "�5private house!"));
		ganghq = createHome(new ItemStack(Material.IRON_DOOR, 1), "�6Gang HQ", Arrays.asList("�5Gang must own", "�5an HQ!"));
		
		inv.setItem(2, town1);
		inv.setItem(3, town2);
		inv.setItem(5, town3);
		inv.setItem(6, town4);
		
		inv.setItem(12, home);
		inv.setItem(14, ganghq);
	}
	
	private ItemStack createTown(DyeColor dc, String name) {
		ItemStack i = new Wool(dc).toItemStack(1);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		i.setItemMeta(im);
		return i;
	}
	
	private ItemStack createHome(ItemStack item, String name, List<String> lore) {
		ItemStack i = item;
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}
	
	public void show(Player p) {
		p.openInventory(inv);
		p.setHealth(20D);
	}
}
