package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class KnockedOutMove implements Listener {
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		
		if (!(e.getFrom().getBlockX() == e.getTo().getBlockX()) && !(e.getFrom().getBlockZ() == e.getTo().getBlockZ())) {
			if (PlayerDataHandler.getInstance().get(player, "knocked out") == true) {
				e.setTo(e.getFrom());
			}
		}
	}
}
