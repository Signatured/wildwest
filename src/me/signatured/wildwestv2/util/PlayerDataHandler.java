package me.signatured.wildwestv2.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class PlayerDataHandler {
	
	private static PlayerDataHandler instance;
	
	public PlayerDataHandler() {
		instance = this;
	}
	
	public static PlayerDataHandler getInstance() {
		return instance;
	}
	
	public void createPlayerData(Player player) {
		File userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("WildWest").getDataFolder(), File.separator + "PlayerData");
        File playerdata = new File(userdata, File.separator + player.getName() + ".yml");
        FileConfiguration playerData = YamlConfiguration.loadConfiguration(playerdata);   
     
        if(!playerdata.exists()){
        	try {
        		playerdata.createNewFile();
        		} catch (IOException e) {
        			e.printStackTrace();
        		}
        		try {
        			try {
        				try {
        					playerData.load(playerdata);
        					loadDefaults(player);
        				} catch (FileNotFoundException e) {
        					e.printStackTrace();
        				} catch (IOException e) {
        					e.printStackTrace();
        				}
        			} catch (InvalidConfigurationException e) {
        				e.printStackTrace();
        			}
        		} finally {
        			
        		}
        }
	}
	
	public void loadDefaults(Player player) {
        
		System.out.println("loaded defaults");
		set(player, "handcuffed", false);
		set(player, "knocked out", false);
		set(player, "prison", false);
		set(player, "spawnselector", true);
		set(player, "ticket", false);
		set(player, "dead", false);
		set(player, "chat", "local");
	}
	
	public void set(Player player, String path, Object o) {
		File userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("WildWest").getDataFolder(), File.separator + "PlayerData");
        File playerdata = new File(userdata, File.separator + player.getName() + ".yml");
        FileConfiguration playerData = YamlConfiguration.loadConfiguration(playerdata);
		createPlayerData(player);
		
		playerData.set(path, o);
		
		try {
			playerData.save(playerdata);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean get(Player player, String s) {
		File userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("WildWest").getDataFolder(), File.separator + "PlayerData");
        File playerdata = new File(userdata, File.separator + player.getName() + ".yml");
        FileConfiguration playerData = YamlConfiguration.loadConfiguration(playerdata);
		
		return playerData.getBoolean(s);
	}
	
	public void save(Player player) {
		File userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("WildWest").getDataFolder(), File.separator + "PlayerData");
        File playerdata = new File(userdata, File.separator + player.getName() + ".yml");
        FileConfiguration playerData = YamlConfiguration.loadConfiguration(playerdata);
		
		try {
			playerData.save(playerdata);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    public int getInt(Player player, String playername, String i) {
    	File userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("WildWest").getDataFolder(), File.separator + "PlayerData");
        File playerdata = new File(userdata, File.separator + player.getName() + ".yml");
        FileConfiguration playerData = YamlConfiguration.loadConfiguration(playerdata);
	    
        return playerData.getInt(i);
    }
    
    public String getString(Player player, String s) {
		File userdata = new File(Bukkit.getServer().getPluginManager().getPlugin("WildWest").getDataFolder(), File.separator + "PlayerData");
        File playerdata = new File(userdata, File.separator + player.getName() + ".yml");
        FileConfiguration playerData = YamlConfiguration.loadConfiguration(playerdata);
		
		return playerData.getString(s);
	}
}
