package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.util.ActionBarHandler;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class SheriffKnockout implements Listener {
	
	@EventHandler (priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			final Player killed = (Player) e.getEntity();
			final Player killer = (Player) e.getDamager();
			
			if (killer.hasPermission("wildwest.sheriff") && killed.hasPermission("sheriff.sheriff")) {
				e.setCancelled(true);
				killer.sendMessage(ChatHandler.getInstance().prefix + "�cYou can't attack other sheriffs!");
			}
			
			if (e.getDamage() >= killed.getHealth()) {
				if (killer.hasPermission("wildwest.sheriff")) {
					if (PlayerDataHandler.getInstance().get(killed, "knocked out") == true) {
						e.setCancelled(true);
						killer.sendMessage(ChatHandler.getInstance().prefix + "�c" + killed.getName() + " is already knocked out!");
						return;
					} else {
						knockout(killed, killer);
					}
				}
			}
		}
	}
	
	public void knockout(Player killed, Player killer) {
		PlayerDataHandler.getInstance().set(killed, "knocked out", true);
		
		TitleHandler.getInstance().sendSubtitleNoFade(killed, "�cYou've been knocked out!");
		TitleHandler.getInstance().sendSubtitleNoFade(killer, "�6Knocked out �8" + killed.getName());
		
		killed.setHealth(1D);
		killed.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 60, 2));
		killed.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 100000, 250));
		killed.setWalkSpeed(0F);
	}
	
	public void knockoutCountdown(Player killed, Player killer) {
		new BukkitRunnable() {
			int countdown = 60;
			
			@Override
			public void run() {
				if (!killed.isOnline()) {
					this.cancel();
					return;
				}
				
				if (countdown <= 0 || PlayerDataHandler.getInstance().get(killed, "knocked out") == false) {
					ActionBarHandler.getInstance().clearActionbar(killed);
					
					if (PlayerDataHandler.getInstance().get(killed, "handcuffed") == true) {
						PlayerDataHandler.getInstance().set(killed, "knocked out", false);
						
						TitleHandler.getInstance().sendSubtitleNoFade(killer, "�8" + killed.getName() + " �chas woken up!");
						TitleHandler.getInstance().sendSubtitleNoFade(killed, "�6You've woken up!");
						
						killed.removePotionEffect(PotionEffectType.BLINDNESS);
						
						this.cancel();
						return;
					} else {
						PlayerDataHandler.getInstance().set(killed, "knocked out", false);
						
						TitleHandler.getInstance().sendSubtitleNoFade(killer, "�8" + killed.getName() + " �chas woken up!");
						TitleHandler.getInstance().sendSubtitleNoFade(killed, "�6You've woken up!");
						
						killed.setWalkSpeed(0.2F);
						killed.removePotionEffect(PotionEffectType.JUMP);
						killed.removePotionEffect(PotionEffectType.BLINDNESS);
						killed.removePotionEffect(PotionEffectType.SLOW);
						
						this.cancel();
						return;
					}
				}
				ActionBarHandler.getInstance().sendActionbarMessage(killed, "�c�lKNOCKEDOUT (" + countdown + ")");
				countdown--;
			}
		}.runTaskTimer(Main.getInstance(), 0, 20);
	}
}
