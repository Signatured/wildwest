package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.effects.Cross;
import me.signatured.wildwestv2.effects.DeathSkeleton;
import me.signatured.wildwestv2.util.ClothingHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class KnockedOutLeave implements Listener {
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		
		if (PlayerDataHandler.getInstance().get(player, "knocked out") == true) {
			Cross.getInstance().cross(player);
			DeathSkeleton.getInstance().spawnSkeleton(player, player.getLocation());
			for (ItemStack is : player.getInventory().getContents()) {
				if (!(is == null)) {
					player.getLocation().getWorld().dropItem(player.getLocation(), is);
				}
			}
			ClothingHandler.getInstance().clearArmor(player);
			player.getInventory().clear();
		}
	}
}
