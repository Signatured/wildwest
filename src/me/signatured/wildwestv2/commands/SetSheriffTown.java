package me.signatured.wildwestv2.commands;

import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.TownCreatorHandler;

import org.bukkit.entity.Player;

public class SetSheriffTown extends WildWestCommand {
	
	@Override
	public String name() {
		return "setsherifftown";
	}
	
	@Override
	public String description() {
		return "Set sheriff town location";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			TownCreatorHandler.getInstance().SheriffTownCreator(sender.getLocation(), sender.getLocation().getPitch(), sender.getLocation().getYaw());
			int size = TownCreatorHandler.getInstance().sherifftowns.size();
			sender.sendMessage("�aSheriff town �c#" + size + " �aset!");
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
