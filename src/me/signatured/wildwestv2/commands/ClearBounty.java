package me.signatured.wildwestv2.commands;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ClearBounty extends WildWestCommand {
	
	@Override
	public String name() {
		return "clearbounty";
	}
	
	@Override
	public String description() {
		return "Clear a players bounty";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			if (args.length > 1) {
				sender.sendMessage("�c/clearbounty <player>");
				return;
			}
			
			if (args.length == 0) {
				BountyPlayer bp = BountyPlayer.get(sender);
				bp.setBounty(0);
				sender.sendMessage("�aBounty cleared!");
				return;
			}
			
			if (args.length == 1) {			
				Player target = Bukkit.getServer().getPlayer(args[0]);
				
				if (target == null) {
					sender.sendMessage("�cPlayer not found!");
					return;
				}
				
				BountyPlayer bp = BountyPlayer.get(target);
				bp.setBounty(0);
				sender.sendMessage("�a" + target + "'s bounty cleared!");
				target.sendMessage("�aBounty cleared!");
			}
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
