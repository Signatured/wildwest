package me.signatured.wildwestv2.commands;

import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ActionBarHandler;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class Handcuff extends WildWestCommand {
	
	@Override
	public String name() {
		return "handcuff";
	}
	
	@Override
	public String description() {
		return "Handcuff a player";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			if (args.length == 0) {
				sender.sendMessage(ChatColor.RED + "Usage: /handcuff {player}");
				return;
			}
			
			if (args.length == 1) {
				Player target = Bukkit.getServer().getPlayer(args[0]);
				
				if (target == null) {
					sender.sendMessage("�c/handcuff <player>");
					return;
				}
				
				if (PlayerDataHandler.getInstance().get(target, "handcuffed") == true) {
					PlayerDataHandler.getInstance().set(target, "handcuffed", false);
					
					TitleHandler.getInstance().sendSubtitleNoFade(sender, "�6Unhandcuffed " + target.getName());
					TitleHandler.getInstance().sendSubtitleNoFade(target, "�6Unhandcuffed by Sheriff " + sender.getName());
					
					target.setWalkSpeed(0.2F);
					target.removePotionEffect(PotionEffectType.JUMP);
				} else {
					PlayerDataHandler.getInstance().set(target, "handcuffed", true);
					
					TitleHandler.getInstance().sendSubtitleNoFade(sender, "�6Handcuffed " + target.getName());
					TitleHandler.getInstance().sendSubtitleNoFade(target, "�6Handcuffed by Sheriff " + sender.getName());
					
					target.setWalkSpeed(0F);
					target.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 100000, 250));
					
					countdown(target);
				}
			}
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
	
	public void countdown(Player player) {
		new BukkitRunnable() {
			int countdown = 600;
			@Override
			public void run(){
				if (!player.isOnline()) {
					this.cancel();
					return;
				}
				
				if (PlayerDataHandler.getInstance().get(player, "handcuffed") == false) {
					ActionBarHandler.getInstance().clearActionbar(player);
					this.cancel();
					return;
				}
            	
				if (countdown <= 0) {
					TitleHandler.getInstance().sendSubtitleNoFade(player, "�6Automatically unhandcuffed");
					PlayerDataHandler.getInstance().set(player, "handcuffed", false);
					this.cancel();
					return;
				}
 
				ActionBarHandler.getInstance().sendActionbarMessage(player, "�c�lHANDCUFFED (" + countdown + ")");
				countdown--;
 
			}
		}.runTaskTimer(Main.getInstance(), 0, 20);
	}
}
