package me.signatured.wildwestv2.util;

import io.puharesource.mc.titlemanager.api.TabTitleObject;

import org.bukkit.entity.Player;

public class TabMenuHandler {
	
	private static TabMenuHandler instance;
	
	public TabMenuHandler() {
		instance = this;
	}
	
	public static TabMenuHandler getInstance() {
		return instance;
	}
	
	public void sendHeaderAndFooter(Player player, String header, String footer) {
		new TabTitleObject(header, footer).send(player);
	}
	
	public void sendHeader(Player player, String header) {
		new TabTitleObject(header, TabTitleObject.Position.HEADER).send(player);
	}
	
	public void sendFooter(Player player, String footer) {
		new TabTitleObject(footer, TabTitleObject.Position.FOOTER).send(player);
	}
}
