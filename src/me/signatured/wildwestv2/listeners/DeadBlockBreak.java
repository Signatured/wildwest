package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class DeadBlockBreak implements Listener {
	
	@EventHandler
	public void onBlockBreak(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		
		if (PlayerDataHandler.getInstance().get(player, "dead") == true) {
			e.setCancelled(true);
		}
	}
}
