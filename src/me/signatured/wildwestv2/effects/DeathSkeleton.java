package me.signatured.wildwestv2.effects;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class DeathSkeleton implements Listener {
	
	private static DeathSkeleton instance;
	
	public DeathSkeleton() {
		instance = this;
	}
	
	public static DeathSkeleton getInstance() {
		return instance;
	}
	
	public void spawnSkeleton(Player player, Location loc) {
		
		Skeleton skele = (Skeleton) player.getWorld().spawnEntity(loc, EntityType.SKELETON);
		skele.setCustomName(player.getName());
		skele.setCustomNameVisible(true);
		
		skele.getEquipment().setItemInHand(player.getItemInHand());
		skele.getEquipment().setHelmet(player.getEquipment().getHelmet());
		skele.getEquipment().setChestplate(player.getEquipment().getChestplate());
		skele.getEquipment().setLeggings(player.getEquipment().getLeggings());
		skele.getEquipment().setBoots(player.getEquipment().getBoots());
		
		skele.damage(20D);
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		Entity entity = e.getEntity();
		if (entity instanceof Skeleton) {
			e.getDrops().clear();
		}
	}
}