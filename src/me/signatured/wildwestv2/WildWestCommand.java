package me.signatured.wildwestv2;

import org.bukkit.entity.Player;

public abstract class WildWestCommand {
	
	public abstract String name();
	public abstract String description();
	public abstract void run(String[] args, Player sender);
}
