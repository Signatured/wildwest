package me.signatured.wildwestv2.commands;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class SetBounty extends WildWestCommand {
	
	@Override
	public String name() {
		return "setbounty";
	}
	
	@Override
	public String description() {
		return "Set a players bounty";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			if (args.length == 0 || args.length > 2) {
				sender.sendMessage("�c/setbounty <amount> <player>");
				return;
			}
			
			if (args.length == 1) {
				if (isInt(args[0], sender)) {
					int num = Integer.parseInt(args[0]);
					BountyPlayer bp = BountyPlayer.get(sender);
					bp.setBounty(num);
					return;
				}
			}
			
			if (args.length == 2) {
				Player target = Bukkit.getServer().getPlayer(args[1]);
				
				if (target == null) {
					sender.sendMessage("�c/setbounty <amount> <player>");
					return;
				}
				
				if (isInt(args[0], sender)) {
					int num = Integer.parseInt(args[0]);
					BountyPlayer bp = BountyPlayer.get(target);
					bp.setBounty(num);
					return;
				}
			}
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
	
	public boolean isInt(String s, Player player) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			player.sendMessage("�c/setbounty <amount> <player>");
			return false;
		}
		return true;
	}
}
