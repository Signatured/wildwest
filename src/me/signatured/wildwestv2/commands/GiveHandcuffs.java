package me.signatured.wildwestv2.commands;

import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GiveHandcuffs extends WildWestCommand {
	
	@Override
	public String name() {
		return "givehandcuffs";
	}
	
	@Override
	public String description() {
		return "Give player handcuffs";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			if (args.length > 1) {
				sender.sendMessage("�c/givehandcuffs <player>");
				return;
			}
			
			if (args.length == 0) {
				Inventory pi = sender.getInventory();
				
				ItemStack handcuffs = new ItemStack(Material.BLAZE_ROD);
				ItemMeta stickmeta = handcuffs.getItemMeta();
				stickmeta.setDisplayName(ChatColor.RED + "Handcuffs");
				handcuffs.setItemMeta(stickmeta);
				pi.addItem(handcuffs);
			}
			
			if (args.length == 1) {
				Player target = Bukkit.getServer().getPlayer(args[0]);
				
				if (target == null) {
					sender.sendMessage("�c/givehandcuffs <player>");
					return;
				}
				
				Inventory pi = target.getInventory();
				
				ItemStack handcuffs = new ItemStack(Material.BLAZE_ROD);
				ItemMeta stickmeta = handcuffs.getItemMeta();
				stickmeta.setDisplayName(ChatColor.RED + "Handcuffs");
				handcuffs.setItemMeta(stickmeta);
				pi.addItem(handcuffs);
			}
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
