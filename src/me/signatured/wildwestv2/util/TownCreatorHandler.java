package me.signatured.wildwestv2.util;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class TownCreatorHandler {
	
	private static TownCreatorHandler instance;
	
	public TownCreatorHandler() {
		instance = this;
	}
	
	public static TownCreatorHandler getInstance() {
		return instance;
	}
		
	public ArrayList<TownCreatorHandler> towns = new ArrayList<TownCreatorHandler>();
	public ArrayList<TownCreatorHandler> sherifftowns = new ArrayList<TownCreatorHandler>();
	
	private Location townlocation;
	private Location sherifftownlocation;
	
	private double townpitch;
	private double townyaw;
	
	private double sheriffpitch;
	private double sheriffyaw;
	
	public void NormalTownCreator(Location tloc, double pitch, double yaw) {
		this.townlocation = tloc;
		this.townpitch = pitch;
		this.townyaw = yaw;
		
		towns.add(this);
	}
	
	public void SheriffTownCreator(Location loc, double pitch, double yaw) {
		this.sherifftownlocation = loc;
		this.sheriffpitch = pitch;
		this.sheriffyaw = yaw;
		
		sherifftowns.add(this);
	}
	
	public Location getTownLocation() {
		return townlocation;
	}
	
	public Location getSheriffTownLocation() {
		return sherifftownlocation;
	}
	
	public double getSheriffTownPitch() {
		return sheriffpitch;
	}
	
	public double getSheriffTownYaw() {
		return sheriffyaw;
	}
	
	public double getTownPitch() {
		return townpitch;
	}
	
	public double getTownYaw() {
		return townyaw;
	}
	
	public void saveTowns() {
		for (int i = 0; i < TownCreatorHandler.getInstance().towns.size(); i++) {
			TownCreatorHandler town = TownCreatorHandler.getInstance().towns.get(i);
			
			Location loc = town.getTownLocation();
			double pitch = town.getTownPitch();
			double yaw = town.getTownYaw();
			
			ConfigHandler.getInstance().getConfig().set("towns." + i + ".x", loc.getX());
			ConfigHandler.getInstance().getConfig().set("towns." + i + ".y", loc.getY());
			ConfigHandler.getInstance().getConfig().set("towns." + i + ".z", loc.getZ());
			ConfigHandler.getInstance().getConfig().set("towns." + i + ".world", loc.getWorld().getName());
			ConfigHandler.getInstance().getConfig().set("towns." + i + ".pitch", pitch);
			ConfigHandler.getInstance().getConfig().set("towns." + i + ".yaw", yaw);
			ConfigHandler.getInstance().save();
			System.out.println("Saved town " + i);
		}
	}
	
	public void saveSheriffTowns() {
		for (int i = 0; i < TownCreatorHandler.getInstance().sherifftowns.size(); i++) {
			TownCreatorHandler town = TownCreatorHandler.getInstance().sherifftowns.get(i);
			
			Location loc = town.getSheriffTownLocation();
			double pitch = town.getSheriffTownPitch();
			double yaw = town.getSheriffTownYaw();
			
			ConfigHandler.getInstance().getConfig().set("sherifftowns." + i + ".x", loc.getX());
			ConfigHandler.getInstance().getConfig().set("sherifftowns." + i + ".y", loc.getY());
			ConfigHandler.getInstance().getConfig().set("sherifftowns." + i + ".z", loc.getZ());
			ConfigHandler.getInstance().getConfig().set("sherifftowns." + i + ".world", loc.getWorld().getName());
			ConfigHandler.getInstance().getConfig().set("sherifftowns." + i + ".pitch", pitch);
			ConfigHandler.getInstance().getConfig().set("sherifftowns." + i + ".yaw", yaw);
			ConfigHandler.getInstance().save();
			System.out.println("Saved sherifftown " + i);
		}
	}
	
	public void loadTowns() {
		if (ConfigHandler.getInstance().getConfig() == null) return;

		if (ConfigHandler.getInstance().getConfig().getConfigurationSection("towns") != null) {
			for(String s : ConfigHandler.getInstance().getConfig().getConfigurationSection("towns").getKeys(false)) {
				double x = ConfigHandler.getInstance().getConfig().getDouble("towns." + s + ".x");
				double y = ConfigHandler.getInstance().getConfig().getDouble("towns." + s + ".y");
				double z = ConfigHandler.getInstance().getConfig().getDouble("towns." + s + ".z");
				World world = Bukkit.getWorld(ConfigHandler.getInstance().getConfig().getString("towns." + s + ".world"));
				double pitch = ConfigHandler.getInstance().getConfig().getDouble("towns." + s + ".pitch");
				double yaw = ConfigHandler.getInstance().getConfig().getDouble("towns." + s + ".yaw");
				NormalTownCreator(new Location(world, x, y, z), pitch, yaw);
			}
		}
	}
	
	public void loadSheriffTowns() {
		if(ConfigHandler.getInstance().getConfig().getConfigurationSection("sherifftowns") == null) return;
		
		for(String s : ConfigHandler.getInstance().getConfig().getConfigurationSection("sherifftowns").getKeys(false)) {
			double x = ConfigHandler.getInstance().getConfig().getDouble("sherifftowns." + s + ".x");
			double y = ConfigHandler.getInstance().getConfig().getDouble("sherifftowns." + s + ".y");
			double z = ConfigHandler.getInstance().getConfig().getDouble("sherifftowns." + s + ".z");
			World world = Bukkit.getWorld(ConfigHandler.getInstance().getConfig().getString("sherifftowns." + s + ".world"));
			double pitch = ConfigHandler.getInstance().getConfig().getDouble("sherifftowns." + s + ".pitch");
			double yaw = ConfigHandler.getInstance().getConfig().getDouble("sherifftowns." + s + ".yaw");
			SheriffTownCreator(new Location(world, x, y, z), pitch, yaw);
		}
	}
}