package me.signatured.wildwestv2.commands;

import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GiveInspector extends WildWestCommand {
	
	@Override
	public String name() {
		return "giveinspector";
	}
	
	@Override
	public String description() {
		return "Give player an inspector";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			if (args.length > 1) {
				sender.sendMessage("�c/giveinspector <player>");
				return;
			}
			
			if (args.length == 0) {
				Inventory pi = sender.getInventory();
				
				ItemStack inspector = new ItemStack(Material.STICK);
				ItemMeta stickmeta = inspector.getItemMeta();
				stickmeta.setDisplayName(ChatColor.BLUE + "Inspector");
				inspector.setItemMeta(stickmeta);
				pi.addItem(inspector);
			}
			
			if (args.length == 1) {
				Player target = Bukkit.getServer().getPlayer(args[0]);
				
				if (target == null) {
					sender.sendMessage("�c/giveinspector <player>");
					return;
				}
				
				Inventory pi = target.getInventory();
				
				ItemStack inspector = new ItemStack(Material.STICK);
				ItemMeta stickmeta = inspector.getItemMeta();
				stickmeta.setDisplayName(ChatColor.BLUE + "Inspector");
				inspector.setItemMeta(stickmeta);
				pi.addItem(inspector);
			}
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
