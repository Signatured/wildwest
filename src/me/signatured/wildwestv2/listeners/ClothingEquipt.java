package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.ClothingHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.PlayerInventory;

public class ClothingEquipt implements Listener {
	
	@EventHandler (priority = EventPriority.LOWEST)
	public void onInventoryClose(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		PlayerInventory inv = player.getInventory();
		
		ClothingHandler.getInstance().checkHelmet(player, inv, e);
		ClothingHandler.getInstance().checkChestplate(player, inv, e);
		ClothingHandler.getInstance().checkLeggings(player, inv, e);
		ClothingHandler.getInstance().checkBoots(player, inv, e);
	}
}
