package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.ActionBarHandler;
import me.signatured.wildwestv2.util.ClothingHandler;
import me.signatured.wildwestv2.util.ConfigHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

public class TicketClick implements Listener {
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		
		if (e.getInventory() != null && e.getInventory().getType() != null &&
				e.getInventory().getType() != InventoryType.CREATIVE && e.getInventory().getName() != null &&
				e.getInventory().getName().endsWith(TicketUse.getInstance().suffix)) {
			if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR &&
					e.getCurrentItem().getItemMeta() != null && e.getCurrentItem().getItemMeta().getDisplayName().contains("Pay Bounty") &&
					!e.getCurrentItem().getItemMeta().getDisplayName().contains("Go to Jail")) {
				payBounty(player);
			} else if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR &&
					e.getCurrentItem().getItemMeta() != null && e.getCurrentItem().getItemMeta().getDisplayName().contains("Go to Jail") &&
					!e.getCurrentItem().getItemMeta().getDisplayName().contains("Pay Bounty")) {
				sendToJail(player);
			}
		}
	}
	
	public void payBounty(Player player) {
		PlayerDataHandler.getInstance().set(player, "handcuffed", false);
		PlayerDataHandler.getInstance().set(player, "ticket", false);
		
		TitleHandler.getInstance().sendSubtitleNoFade(player, "�6Ticket Paid!");
		
		player.setWalkSpeed(0.2F);
		player.removePotionEffect(PotionEffectType.JUMP);
		player.removePotionEffect(PotionEffectType.BLINDNESS);
		player.removePotionEffect(PotionEffectType.SLOW);
		
		player.closeInventory();
	}
	
	public static void sendToJail(Player player) {
		Inventory pi = player.getInventory();
		
		PlayerDataHandler.getInstance().set(player, "handcuffed", false);
		PlayerDataHandler.getInstance().set(player, "knocked out", false);
		PlayerDataHandler.getInstance().set(player, "ticket", false);
		PlayerDataHandler.getInstance().set(player, "prison", true);
		
		player.setWalkSpeed(0.2F);
		player.removePotionEffect(PotionEffectType.JUMP);
		player.removePotionEffect(PotionEffectType.BLINDNESS);
		player.removePotionEffect(PotionEffectType.SLOW);
		
		pi.clear();
		ClothingHandler.getInstance().clearArmor(player);
		player.setHealth(20D);
		
		World w = Bukkit.getWorld(ConfigHandler.getInstance().getConfig().getString("prison.world"));
		double x = ConfigHandler.getInstance().getConfig().getDouble("prison.x");
		double y = ConfigHandler.getInstance().getConfig().getDouble("prison.y");
		double z = ConfigHandler.getInstance().getConfig().getDouble("prison.z");
		float pitch = (float) ConfigHandler.getInstance().getConfig().getDouble("prison.pitch");
		float yaw = (float) ConfigHandler.getInstance().getConfig().getDouble("prison.yaw");
		
		ItemStack pick = new ItemStack(Material.DIAMOND_PICKAXE, 1);
		ItemMeta pickmeta = pick.getItemMeta();
		
		Location loc = new Location (w, x, y, z);
		loc.setYaw(yaw);
		player.getLocation().setPitch(pitch);
		player.teleport(loc);
		
		pickmeta.setDisplayName(ChatColor.AQUA + "Diamond Pickaxe");
		pick.setItemMeta(pickmeta);
		player.getInventory().setItem(0, pick);
		
		ClothingHandler.getInstance().giveJailArmor(player);
		
		TitleHandler.getInstance().sendTitleAndSubtitleNoFade(player, "�8�lJAILED!", "�6Pay off your bounty!");
		ActionBarHandler.getInstance().sendActionbarMessage(player, "�6Mine ores to pay off your bounty!");
	}
}
