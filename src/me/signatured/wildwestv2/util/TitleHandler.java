package me.signatured.wildwestv2.util;

import io.puharesource.mc.titlemanager.api.TitleObject;

import org.bukkit.entity.Player;

public class TitleHandler {
	
	private static TitleHandler instance;
	
	public TitleHandler() {
		instance = this;
	}
	
	public static TitleHandler getInstance() {
		return instance;
	}
	
	public void sendTitleAndSubtitleNoFade(Player player, String title, String subtitle) {
		new TitleObject(title, subtitle).setFadeIn(0).setStay(40).setFadeOut(0).send(player);
	}
	
	public void sendTitleAndSubtitle(Player player, String title, String subtitle, int fadein, int stay, int fadeout) {
		new TitleObject(title, subtitle).setFadeIn(fadein).setStay(stay).setFadeOut(fadeout).send(player);
	}
	
	public void sendTitleNoFade(Player player, String title) {
		new TitleObject(title, TitleObject.TitleType.TITLE).setFadeIn(0).setStay(40).setFadeOut(0).send(player);
	}
	
	public void sendTitle(Player player, String title, int fadein, int stay, int fadeout) {
		new TitleObject(title, TitleObject.TitleType.TITLE).setFadeIn(fadein).setStay(stay).setFadeOut(fadeout).send(player);
	}
	
	public void sendSubtitleNoFade(Player player, String subtitle) {
		new TitleObject(subtitle, TitleObject.TitleType.SUBTITLE).setFadeIn(0).setStay(40).setFadeOut(0).send(player);
	}
	
	public void sendSubtitle(Player player, String subtitle, int fadein, int stay, int fadeout) {
		new TitleObject(subtitle, TitleObject.TitleType.SUBTITLE).setFadeIn(fadein).setStay(stay).setFadeOut(fadeout).send(player);
	}
	
	public void clearTitleAndSubtitle(Player player) {
		new TitleObject(" ", " ").send(player);
	}
}
