package me.signatured.wildwestv2.util;

import me.signatured.wildwestv2.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

public class HoloHandler {
	
	private static HoloHandler instance;
	
	public HoloHandler() {
		instance = this;
	}
	
	public static HoloHandler getInstance() {
		return instance;
	}
	
	Plugin plugin = Main.getInstance();
	
	public void createCrossHolo(Location loc, String text) {
		Hologram hologram = HologramsAPI.createHologram(plugin, loc);
		hologram.appendTextLine(text);
		
		Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			@Override
			public void run() {
				deleteIfOld(hologram);
			}
		}, 20 * 30);
	}
	
	public void createTwoLineHolo(Location loc, String line1, String line2) {
		Hologram hologram = HologramsAPI.createHologram(plugin, loc);
		hologram.appendTextLine(line1);
		hologram.appendTextLine(line2);
	}
	
	public void deleteIfOld(Hologram hologram) {

		long tenMinutesMillis = 30 * 1000; // 30 seconds in milliseconds
		long elapsedMillis = System.currentTimeMillis() - hologram.getCreationTimestamp(); // Milliseconds elapsed from the creation of the hologram

		if (elapsedMillis > tenMinutesMillis) {
			hologram.delete();
		}
	}
}
