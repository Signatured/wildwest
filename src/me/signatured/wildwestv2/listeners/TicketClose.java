package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.Main;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class TicketClose implements Listener {
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		final Player player = (Player) e.getPlayer();
		
		if (e.getInventory().getName().endsWith(TicketUse.getInstance().suffix)) {
			Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				@Override
				public void run() {
					TicketUse.getInstance().show(player);
				}
			}, 3);
		}
	}
}
