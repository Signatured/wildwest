package me.signatured.wildwestv2.commands;

import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;

public class Channel extends WildWestCommand {
	
	@Override
	public String name() {
		return "channel";
	}
	
	@Override
	public String description() {
		return "Switch chat channels";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (args.length == 0 || args.length > 1) {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�c/channel [global/local/gang]");
			return;
		} else if (args.length == 1) {
			if (args[0].equalsIgnoreCase("global")) {
				if (PlayerDataHandler.getInstance().getString(sender, "chat") == "global") {
					sender.sendMessage(ChatHandler.getInstance().prefix + "�cYou're already in global chat!");
					return;
				} else {
					PlayerDataHandler.getInstance().set(sender, "chat", "global");
					sender.sendMessage(ChatHandler.getInstance().prefix + "�6Set channel to global!");
				}
			} else if (args[0].equalsIgnoreCase("local")) {
				if (PlayerDataHandler.getInstance().getString(sender, "chat") == "local") {
					sender.sendMessage(ChatHandler.getInstance().prefix + "�cYou're already in local chat!");
					return;
				} else {
					PlayerDataHandler.getInstance().set(sender, "chat", "local");
					sender.sendMessage(ChatHandler.getInstance().prefix + "�6Set channel to local!");
				}
			}
		}
	}
}
