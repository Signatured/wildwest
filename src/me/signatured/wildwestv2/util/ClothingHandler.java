package me.signatured.wildwestv2.util;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class ClothingHandler {
	
	private static ClothingHandler instance;
	
	public ClothingHandler() {
		instance = this;
	}
	
	public static ClothingHandler getInstance() {
		return instance;
	}
	
	public void clearArmor(Player player) {
		player.getInventory().setHelmet(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setLeggings(null);
		player.getInventory().setBoots(null);		
	}
	
	public void giveBanditHat(Player player) {
		ItemStack cbhat = new ItemStack(Material.LEATHER, 1);
		ItemMeta hatmeta = cbhat.getItemMeta();
		
		hatmeta.setDisplayName("�eCowboy Hat");
		cbhat.setItemMeta(hatmeta);
		
		player.getInventory().setHelmet(cbhat);
	}
	
	public void giveSheriffHat(Player player) {
		ItemStack shat = new ItemStack(Material.CLAY_BRICK, 1);
		ItemMeta hatmeta = shat.getItemMeta();
		
		hatmeta.setDisplayName("�cSheriff Hat");
		shat.setItemMeta(hatmeta);
		
		player.getInventory().setHelmet(shat);
	}
	
	public void giveVIPHat(Player player) {
		ItemStack vhat = new ItemStack(Material.CLAY_BALL, 1);
		ItemMeta hatmeta = vhat.getItemMeta();
		
		hatmeta.setDisplayName("�bVIP Hat");
		vhat.setItemMeta(hatmeta);
		
		player.getInventory().setHelmet(vhat);
	}
	
	public void giveMVPHat(Player player) {
		ItemStack mhat = new ItemStack(Material.BOWL, 1);
		ItemMeta hatmeta = mhat.getItemMeta();
		
		hatmeta.setDisplayName("�9MVP Hat");
		mhat.setItemMeta(hatmeta);
		
		player.getInventory().setHelmet(mhat);
	}
	
	public void giveJailHat(Player player) {
		ItemStack jhat = new ItemStack(Material.INK_SACK, 1);
		ItemMeta hatmeta = jhat.getItemMeta();
		
		hatmeta.setDisplayName("�7Prison Suit");
		jhat.setItemMeta(hatmeta);
		
		player.getInventory().setHelmet(jhat);
	}
	
	@SuppressWarnings("deprecation")
	public void checkHelmet(Player player, PlayerInventory inv, InventoryClickEvent e) {
		if (e.getCursor() != null) {
			if (e.getSlotType() == SlotType.ARMOR) {
				ItemStack item = e.getCursor();
				switch (item.getType()) {
				
				case BOWL:
					if (!player.hasPermission("wildwest.mvp")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be MVP to wear this item!");
					} else {
						ItemStack oldhelm = inv.getHelmet();
						
						inv.setHelmet(e.getCursor());
						e.setCursor(oldhelm);
					}
					break;
					
				case CLAY_BALL:
					if (!player.hasPermission("wildwest.vip")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be VIP+ to wear this item!");
					} else {
						ItemStack oldhelm = inv.getHelmet();
						
						inv.setHelmet(e.getCursor());
						e.setCursor(oldhelm);
					}
					break;
					
				case CLAY_BRICK:
					if (!player.hasPermission("wildwest.sheriff")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be a sheriff to wear this item!");
					} else {
						ItemStack oldhelm = inv.getHelmet();
						
						inv.setHelmet(e.getCursor());
						e.setCursor(oldhelm);
					}
					break;
					
				case LEATHER:
					ItemStack oldhelm = inv.getHelmet();
					
					inv.setHelmet(e.getCursor());
					e.setCursor(oldhelm);
					break;
					
				case INK_SACK:
					e.setCancelled(true);
					break;
					
				default:
					break;
				}
			}
		}
	}
	
	public void checkChestplate(Player player, PlayerInventory inv, InventoryClickEvent e) {
		if (e.getCursor() != null) {
			if (e.getSlotType() == SlotType.ARMOR) {
				ItemStack item = e.getCursor();
				switch (item.getType()) {
				
				case DIAMOND_CHESTPLATE:
					if (!player.hasPermission("wildwest.mvp")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be MVP to wear this item!");
					}
					break;
					
				case GOLD_CHESTPLATE:
					if (!player.hasPermission("wildwest.vip")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be VIP+ to wear this item!");
					}
					break;
					
				case IRON_CHESTPLATE:
					if (!player.hasPermission("wildwest.sheriff")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be a sheriff to wear this item!");
					}
					break;
					
				case CHAINMAIL_CHESTPLATE:
					e.setCancelled(true);
					break;
					
				default:
					break;
				}
			}
		}
	}
	
	public void checkLeggings(Player player, PlayerInventory inv, InventoryClickEvent e) {
		if (e.getCursor() != null) {
			if (e.getSlotType() == SlotType.ARMOR) {
				ItemStack item = e.getCursor();
				switch (item.getType()) {
				
				case DIAMOND_LEGGINGS:
					if (!player.hasPermission("wildwest.mvp")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be MVP to wear this item!");
					}
					break;
					
				case GOLD_LEGGINGS:
					if (!player.hasPermission("wildwest.vip")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be VIP+ to wear this item!");
					}
					break;
					
				case IRON_LEGGINGS:
					if (!player.hasPermission("wildwest.sheriff")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be a sheriff to wear this item!");
					}
					break;
					
				case CHAINMAIL_LEGGINGS:
					e.setCancelled(true);
					break;
					
				default:
					break;
				}
			}
		}
	}
	
	public void checkBoots(Player player, PlayerInventory inv, InventoryClickEvent e) {
		if (e.getCursor() != null) {
			if (e.getSlotType() == SlotType.ARMOR) {
				ItemStack item = e.getCursor();
				switch (item.getType()) {
				
				case DIAMOND_BOOTS:
					if (!player.hasPermission("wildwest.mvp")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be MVP to wear this item!");
					}
					break;
					
				case GOLD_BOOTS:
					if (!player.hasPermission("wildwest.vip")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be VIP+ to wear this item!");
					}
					break;
					
				case IRON_BOOTS:
					if (!player.hasPermission("wildwest.sheriff")) {
						e.setCancelled(true);
						player.closeInventory();
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou must be a sheriff to wear this item!");
					}
					break;
					
				case CHAINMAIL_BOOTS:
					e.setCancelled(true);
					break;
					
				default:
					break;
				}
			}
		}
	}
	
	public void giveBanditArmor(Player player) {
		
		ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		ItemStack boots = new ItemStack(Material.LEATHER_BOOTS, 1);
		
		ItemMeta chestplatemeta = chestplate.getItemMeta();
		ItemMeta leggingsmeta = leggings.getItemMeta();
		ItemMeta bootsmeta = boots.getItemMeta();
		
		chestplatemeta.setDisplayName(ChatColor.YELLOW + "Bandit Chestplate");
		leggingsmeta.setDisplayName(ChatColor.YELLOW + "Bandit Leggings");
		bootsmeta.setDisplayName(ChatColor.YELLOW + "Bandit Boots");
		
		chestplate.setItemMeta(chestplatemeta);
		leggings.setItemMeta(leggingsmeta);
		boots.setItemMeta(bootsmeta);
		
		player.getInventory().setChestplate(chestplate);
		player.getInventory().setLeggings(leggings);
		player.getInventory().setBoots(boots);
	}
	
	public void giveVIPArmor(Player player) {
		
		ItemStack chestplate = new ItemStack(Material.GOLD_CHESTPLATE, 1);
		ItemStack leggings = new ItemStack(Material.GOLD_LEGGINGS, 1);
		ItemStack boots = new ItemStack(Material.GOLD_BOOTS, 1);
		
		ItemMeta chestplatemeta = chestplate.getItemMeta();
		ItemMeta leggingsmeta = leggings.getItemMeta();
		ItemMeta bootsmeta = boots.getItemMeta();
		
		chestplatemeta.setDisplayName(ChatColor.AQUA + "VIP Chestplate");
		leggingsmeta.setDisplayName(ChatColor.AQUA + "VIP Leggings");
		bootsmeta.setDisplayName(ChatColor.AQUA + "VIP Boots");
		
		chestplate.setItemMeta(chestplatemeta);
		leggings.setItemMeta(leggingsmeta);
		boots.setItemMeta(bootsmeta);
		
		player.getInventory().setChestplate(chestplate);
		player.getInventory().setLeggings(leggings);
		player.getInventory().setBoots(boots);
	}

	public void giveMVPArmor(Player player) {
	
		ItemStack chestplate = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
		ItemStack leggings = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
		ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS, 1);
	
		ItemMeta chestplatemeta = chestplate.getItemMeta();
		ItemMeta leggingsmeta = leggings.getItemMeta();
		ItemMeta bootsmeta = boots.getItemMeta();
	
		chestplatemeta.setDisplayName(ChatColor.BLUE + "MVP Chestplate");
		leggingsmeta.setDisplayName(ChatColor.BLUE + "MVP Leggings");
		bootsmeta.setDisplayName(ChatColor.BLUE + "MVP Boots");
	
		chestplate.setItemMeta(chestplatemeta);
		leggings.setItemMeta(leggingsmeta);
		boots.setItemMeta(bootsmeta);
	
		player.getInventory().setChestplate(chestplate);
		player.getInventory().setLeggings(leggings);
		player.getInventory().setBoots(boots);
	}
	
	public void giveSheriffArmor(Player player) {
		
		ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE, 1);
		ItemStack leggings = new ItemStack(Material.IRON_LEGGINGS, 1);
		ItemStack boots = new ItemStack(Material.IRON_BOOTS, 1);
	
		ItemMeta chestplatemeta = chestplate.getItemMeta();
		ItemMeta leggingsmeta = leggings.getItemMeta();
		ItemMeta bootsmeta = boots.getItemMeta();
	
		chestplatemeta.setDisplayName(ChatColor.RED + "Sheriff Chestplate");
		leggingsmeta.setDisplayName(ChatColor.RED + "Sheriff Leggings");
		bootsmeta.setDisplayName(ChatColor.RED + "Sheriff Boots");
	
		chestplate.setItemMeta(chestplatemeta);
		leggings.setItemMeta(leggingsmeta);
		boots.setItemMeta(bootsmeta);
	
		player.getInventory().setChestplate(chestplate);
		player.getInventory().setLeggings(leggings);
		player.getInventory().setBoots(boots);
	}
	
	public void giveJailArmor(Player player) {
		
		giveJailHat(player);
		
		ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		LeatherArmorMeta cim = (LeatherArmorMeta) chestplate.getItemMeta();
		cim.setColor(Color.BLACK);
		chestplate.setItemMeta(cim);
		
		ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		LeatherArmorMeta lim = (LeatherArmorMeta) chestplate.getItemMeta();
		lim.setColor(Color.WHITE);
		chestplate.setItemMeta(lim);
		
		ItemStack boots = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta bim = (LeatherArmorMeta) chestplate.getItemMeta();
		bim.setColor(Color.BLACK);
		chestplate.setItemMeta(bim);
		
		cim.setDisplayName(ChatColor.GRAY + "Prison Suit");
		lim.setDisplayName(ChatColor.GRAY + "Prison Suit");
		bim.setDisplayName(ChatColor.GRAY + "Prison Suit");
		
		chestplate.setItemMeta(cim);
		leggings.setItemMeta(lim);
		boots.setItemMeta(bim);
		
		player.getInventory().setChestplate(chestplate);
		player.getInventory().setLeggings(leggings);
		player.getInventory().setBoots(boots);
	}
	
	public void giveDefaultGear(Player player) {
		if (PlayerDataHandler.getInstance().get(player, "prison") == true) {
			giveJailHat(player);
			giveJailArmor(player);
		} else {
			if (player.hasPermission("wildwest.mvp")) {
				if (player.hasPermission("wildwest.sheriff")) {
					giveSheriffHat(player);
					giveSheriffArmor(player);
				} else {
					giveMVPHat(player);
					giveMVPArmor(player);
				}
			} else if (player.hasPermission("wildwest.vip")) {
				if (player.hasPermission("wildwest.sheriff")) {
					giveSheriffHat(player);
					giveSheriffArmor(player);
				} else {
					giveVIPHat(player);
					giveMVPArmor(player);
				}
			} else if (player.hasPermission("wildwest.sheriff")) {
				if (!player.hasPermission("wildwest.vip") && !player.hasPermission("wildwest.mvp")) {
					giveSheriffHat(player);
					giveSheriffArmor(player);
				}
			} else {
				giveBanditHat(player);
				giveBanditArmor(player);
			}
		}
	}
}
