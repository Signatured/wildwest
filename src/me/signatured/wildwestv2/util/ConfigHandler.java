package me.signatured.wildwestv2.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.signatured.wildwestv2.Main;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigHandler {
	
	private static ConfigHandler instance;
	
	public static ConfigHandler getInstance() {
		return instance;
	}
	
	public Main plugin;
	public File cfile;
	public YamlConfiguration config;
	
	public ConfigHandler(Main plugin) {
		instance = this;
		try {
			this.plugin = plugin;
			
			if (!this.plugin.getDataFolder().exists()) {
				this.plugin.getDataFolder().mkdir();
			}
			
			cfile = new File(this.plugin.getDataFolder(), "config.yml");
			
			if (cfile.exists()) {
				config = YamlConfiguration.loadConfiguration(cfile);
			} else {
				cfile.createNewFile();
				config = YamlConfiguration.loadConfiguration(cfile);
				loadDefaults();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadDefaults() {
		List<String> contraband = new ArrayList<String>();
    	contraband.add("IRON_SPADE");
    	contraband.add("IRON_PICKAXE");
    	
    	if (config.contains("contraband") == false) {
    		config.set("contraband", contraband);
    		save();
    	}
	}
	
	public void set(String path, Object o) {
		config.set(path, o);
		save();
	}
	
	public Object get(String path) {
		return config.get(path);
	}
	
	public void save() {
		try {
			config.save(cfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getConfig() {
		return config;
	}

}
