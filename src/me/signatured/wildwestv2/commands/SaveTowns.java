package me.signatured.wildwestv2.commands;

import org.bukkit.entity.Player;

import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.TownCreatorHandler;

public class SaveTowns extends WildWestCommand {
	
	@Override
	public String name() {
		return "savetowns";
	}
	
	@Override
	public String description() {
		return "Save towns";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			TownCreatorHandler.getInstance().saveTowns();
			TownCreatorHandler.getInstance().saveSheriffTowns();
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
