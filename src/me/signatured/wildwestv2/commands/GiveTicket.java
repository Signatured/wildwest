package me.signatured.wildwestv2.commands;

import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GiveTicket extends WildWestCommand {
	
	@Override
	public String name() {
		return "giveticket";
	}
	
	@Override
	public String description() {
		return "Give player a ticket";
	}
	
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			if (args.length > 1) {
				sender.sendMessage("�c/giveticket <player>");
				return;
			}
			
			if (args.length == 0) {
				Inventory pi = sender.getInventory();
				
				ItemStack ticket = new ItemStack(Material.PAPER);
				ItemMeta ticketmeta = ticket.getItemMeta();
				ticketmeta.setDisplayName(ChatColor.YELLOW + "Ticket");
				ticket.setItemMeta(ticketmeta);
				pi.addItem(ticket);
			}
			
			if (args.length == 1) {
				Player target = Bukkit.getServer().getPlayer(args[0]);
				
				if (target == null) {
					sender.sendMessage("�c/giveticket <player>");
					return;
				}
				
				Inventory pi = target.getInventory();
				
				ItemStack ticket = new ItemStack(Material.PAPER);
				ItemMeta ticketmeta = ticket.getItemMeta();
				ticketmeta.setDisplayName(ChatColor.YELLOW + "Ticket");
				ticket.setItemMeta(ticketmeta);
				pi.addItem(ticket);
			}
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
