package me.signatured.wildwestv2.listeners;

import java.util.List;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.ConfigHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InspectorUse implements Listener {
	
	@EventHandler
	public void onInspectorUse(PlayerInteractEntityEvent e) {
		Player player = e.getPlayer();
		List<String> contraband = ConfigHandler.getInstance().getConfig().getStringList("contraband");
		
		if (e.getRightClicked() instanceof Player) {
			Player clicked = (Player) e.getRightClicked();
			
			if (player.getItemInHand().getType() == Material.STICK &&
					player.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.BLUE + "Inspector")) {
				if (PlayerDataHandler.getInstance().get(player, "handcuffed") == true) {
					Inventory inv = clicked.getInventory();
					boolean found = false;
					
					for (ItemStack is : inv.getContents()) {
						if (is != null && contraband.contains(is.getType().name())) {
							found = true;
							break;
						}
					}
					
					if (found) {
						for (ItemStack is : inv.getContents()) {
							BountyPlayer bp = BountyPlayer.get(clicked);
							
							if (is != null) {
								switch (is.getType()) {
								
								case IRON_SPADE:
									bp.setBounty(bp.getBounty() + 100);
									is.setType(Material.AIR);
									break;
								
								default:
									break;
								}
							}
						}
						player.sendMessage(ChatHandler.getInstance().prefix + "�8" + clicked.getName() + " �6had illegal items! Book 'em!");
					} else {
						player.sendMessage(ChatHandler.getInstance().prefix + "�8" + clicked.getName() + " �6doesn't have illegal items on hand.");
					}
				}
			}
		}
	}
}
