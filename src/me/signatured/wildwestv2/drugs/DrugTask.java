package me.signatured.wildwestv2.drugs;

public class DrugTask implements Runnable {
	
	public void run() {
		for (DrugSpawn drug : DrugSpawn.drugs) {
			if (drug.getLocation().getBlock().getType() == drug.getType()) continue;
			
			if (drug.reset <= 0) {
				drug.getLocation().getBlock().setType(drug.getType());
				drug.reset = 30;
				continue;
			}
			
			drug.reset--;
		}
	}
}
