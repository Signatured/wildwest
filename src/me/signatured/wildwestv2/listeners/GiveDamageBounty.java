package me.signatured.wildwestv2.listeners;

import java.util.HashMap;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.util.ActionBarHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class GiveDamageBounty implements Listener {
	
	public static GiveDamageBounty instance;
	
	public GiveDamageBounty() {
		instance = this;
	}
	
	public static GiveDamageBounty getInstance() {
		return instance;
	}
	
	public HashMap<Player, Player> tagged = new HashMap<Player, Player>();
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player damaged = (Player) e.getEntity();
			Player damager = (Player) e.getDamager();
			BountyPlayer bp = BountyPlayer.get(damager);
			
			if (!damager.hasPermission("wildwest.sheriff")) {
				if (tagged.get(damager) == null || tagged.get(damager) != damaged) {
					if (damaged.hasPermission("wildwest.sheriff")) {
						bp.setBounty(bp.getBounty() + 300);
						ActionBarHandler.getInstance().sendActionbarMessage(damager, "�4�l+300 Bounty");
						tagged.put(damager, damaged);
					} else {
						bp.setBounty(bp.getBounty() + 150);
						ActionBarHandler.getInstance().sendActionbarMessage(damager, "�4�l+150 Bounty");
						tagged.put(damager, damaged);
					}
				}
			}
		}
	}
}
