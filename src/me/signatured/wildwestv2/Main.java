package me.signatured.wildwestv2;

import java.util.Map.Entry;

import me.signatured.wildwestv2.drugs.DrugSpawn;
import me.signatured.wildwestv2.drugs.DrugTask;
import me.signatured.wildwestv2.effects.Cross;
import me.signatured.wildwestv2.effects.DeathSkeleton;
import me.signatured.wildwestv2.listeners.BanditDeath;
import me.signatured.wildwestv2.listeners.Chat;
import me.signatured.wildwestv2.listeners.ClothingEquipt;
import me.signatured.wildwestv2.listeners.DeadAttack;
import me.signatured.wildwestv2.listeners.DeadBlockBreak;
import me.signatured.wildwestv2.listeners.DeadDamaged;
import me.signatured.wildwestv2.listeners.DeadPickupItem;
import me.signatured.wildwestv2.listeners.DeadRegeneration;
import me.signatured.wildwestv2.listeners.DrugHarvest;
import me.signatured.wildwestv2.listeners.DrugPlant;
import me.signatured.wildwestv2.listeners.GiveDamageBounty;
import me.signatured.wildwestv2.listeners.GiveDeathBounty;
import me.signatured.wildwestv2.listeners.HandcuffAttack;
import me.signatured.wildwestv2.listeners.HandcuffGunUse;
import me.signatured.wildwestv2.listeners.HandcuffLeave;
import me.signatured.wildwestv2.listeners.HandcuffMove;
import me.signatured.wildwestv2.listeners.HandcuffPickupItem;
import me.signatured.wildwestv2.listeners.HandcuffThrowItem;
import me.signatured.wildwestv2.listeners.HandcuffUse;
import me.signatured.wildwestv2.listeners.InspectorUse;
import me.signatured.wildwestv2.listeners.KnockedOutAttack;
import me.signatured.wildwestv2.listeners.KnockedOutLeave;
import me.signatured.wildwestv2.listeners.KnockedOutMove;
import me.signatured.wildwestv2.listeners.OreMine;
import me.signatured.wildwestv2.listeners.PlayerJoin;
import me.signatured.wildwestv2.listeners.PrisonDamage;
import me.signatured.wildwestv2.listeners.PrisonInventoryClick;
import me.signatured.wildwestv2.listeners.PrisonItemDrop;
import me.signatured.wildwestv2.listeners.PrisonOreMine;
import me.signatured.wildwestv2.listeners.Regeneration;
import me.signatured.wildwestv2.listeners.SheriffDeath;
import me.signatured.wildwestv2.listeners.SheriffInventoryClick;
import me.signatured.wildwestv2.listeners.SheriffKnockout;
import me.signatured.wildwestv2.listeners.SheriffThrowItem;
import me.signatured.wildwestv2.listeners.SpawnSelectorClick;
import me.signatured.wildwestv2.listeners.SpawnSelectorClose;
import me.signatured.wildwestv2.listeners.TicketClick;
import me.signatured.wildwestv2.listeners.TicketClose;
import me.signatured.wildwestv2.listeners.TicketUse;
import me.signatured.wildwestv2.spawnselectors.BanditSelector;
import me.signatured.wildwestv2.spawnselectors.SheriffSelector;
import me.signatured.wildwestv2.util.ActionBarHandler;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.ClothingHandler;
import me.signatured.wildwestv2.util.ConfigHandler;
import me.signatured.wildwestv2.util.DrugHandler;
import me.signatured.wildwestv2.util.GunsHandler;
import me.signatured.wildwestv2.util.HoloHandler;
import me.signatured.wildwestv2.util.JailUtil;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TabMenuHandler;
import me.signatured.wildwestv2.util.TitleHandler;
import me.signatured.wildwestv2.util.TownCreatorHandler;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class Main extends JavaPlugin {
	
	private static Main instance;
	
	public Main() {
		instance = this;
	}
	
	public static Main getInstance() {
		return instance;
	}
	
	public static ConfigHandler ch;
	public static GunsHandler gh;
	public static DrugHandler dh;
	
	@Override
	public void onEnable() {
		
		if (getWorldGuard() == null || getWorldEdit() == null) {
			this.setEnabled(false);
		}
		
		ch = new ConfigHandler(this);
		gh = new GunsHandler(this);
		dh = new DrugHandler(this);

		registerConstructors();
		registerEvents();
		
		getCommand("wildwest").setExecutor(new CommandManager());
		
		TownCreatorHandler.getInstance().loadTowns();
		TownCreatorHandler.getInstance().loadSheriffTowns();
		
		if (getServer().getPluginManager().getPlugin("TitleManager") == null || !getServer().getPluginManager().getPlugin("TitleManager").isEnabled()) {
			getLogger().warning("Failed to hook into TitleManager, disabling plugin!");
			getPluginLoader().disablePlugin(this);
		}
		
		if (!Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
	        getLogger().severe("*** HolographicDisplays is not installed or not enabled. ***");
	        getLogger().severe("*** This plugin will be disabled. ***");
	        this.setEnabled(false);
	        return;
	    }
		
		new BukkitRunnable() {
		    @Override
		    public void run() {
		        for (Hologram hologram : HologramsAPI.getHolograms(Main.getInstance())) {
		            HoloHandler.getInstance().deleteIfOld(hologram);
		        }
		    }

		}.runTaskTimer(Main.getInstance(), 20, 20);
		
		Bukkit.getScheduler().runTaskTimer(this, new DrugTask(), 20L, 20L);
		
		FileConfiguration drugsfile = DrugHandler.getInstance().getDrugs();
		if (drugsfile.getConfigurationSection("drugs") == null) {
			drugsfile.createSection("drugs");
		} else {
			for (String s : drugsfile.getConfigurationSection("drugs").getKeys(false)) {
				double x = drugsfile.getDouble("drugs." + s + ".x");
				double y = drugsfile.getDouble("drugs." + s + ".y");
				double z = drugsfile.getDouble("drugs." + s + ".z");
				World world = Bukkit.getWorld(drugsfile.getString("drugs." + s + ".world"));
				
				Location loc = new Location(world, x, y, z);
				
				Material type = Material.valueOf(drugsfile.getString("drugs." + s + ".type"));
				
				DrugSpawn spawn = new DrugSpawn(type, loc);
				spawn.getLocation().getBlock().setType(spawn.getType());
			}
		}
	}
	
	@Override
	public void onDisable() {
		for (Hologram hologram : HologramsAPI.getHolograms(Main.getInstance())) {
            deleteIfOld(hologram);
        }
		
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			PlayerDataHandler.getInstance().set(player, "knocked out", false);
			PlayerDataHandler.getInstance().set(player, "handcuffed", false);
			
			player.removePotionEffect(PotionEffectType.JUMP);
			player.removePotionEffect(PotionEffectType.BLINDNESS);
			player.setWalkSpeed(0.2F);
			
			for (Entry<Player, Location> crosses : Cross.getInstance().crosses.entrySet()) {
				crosses.getValue().getBlock().setType(Material.AIR);
			}
		}
		
		DrugHandler.getInstance().saveDrugs();
	}
	
	public void deleteIfOld(Hologram hologram) {

	    long tenMinutesMillis = 1000; // Ten minutes in milliseconds
	    long elapsedMillis = System.currentTimeMillis() - hologram.getCreationTimestamp(); // Milliseconds elapsed from the creation of the hologram

	    if (elapsedMillis > tenMinutesMillis) {
	        hologram.delete();
	    }
	}
	
	public void registerConstructors() {
		new Cross();
		new DeathSkeleton();
		new BanditDeath();
		new GiveDamageBounty();
		new TicketUse();
		new BanditSelector();
		new SheriffSelector();
		new ActionBarHandler();
		new ChatHandler();
		new ClothingHandler();
		new HoloHandler();
		new JailUtil();
		new PlayerDataHandler();
		new TabMenuHandler();
		new TitleHandler();
		new TownCreatorHandler();
	}
	
	public void registerEvents() {
		Bukkit.getServer().getPluginManager().registerEvents(new DeathSkeleton(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new BanditDeath(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Chat(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ClothingEquipt(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new DeadAttack(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new DeadBlockBreak(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new DeadDamaged(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new DeadRegeneration(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new DrugHarvest(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new DrugPlant(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new DeadPickupItem(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new GiveDamageBounty(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new GiveDeathBounty(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new HandcuffAttack(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new HandcuffGunUse(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new HandcuffLeave(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new HandcuffMove(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new HandcuffPickupItem(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new HandcuffThrowItem(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new HandcuffUse(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new InspectorUse(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new KnockedOutAttack(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new KnockedOutLeave(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new KnockedOutMove(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new OreMine(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PrisonDamage(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PrisonInventoryClick(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PrisonItemDrop(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PrisonOreMine(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Regeneration(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SheriffDeath(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SheriffInventoryClick(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SheriffKnockout(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SheriffThrowItem(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new TicketClick(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new TicketClose(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new TicketUse(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new HandcuffThrowItem(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SpawnSelectorClose(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SpawnSelectorClick(), this);
	}
	
	public WorldGuardPlugin getWorldGuard() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
     
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null;
        }
     
        return (WorldGuardPlugin) plugin;
    }
	
	public WorldEditPlugin getWorldEdit() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
         
    if (plugin == null || !(plugin instanceof WorldEditPlugin)) {
        return null;
    }
 
    return (WorldEditPlugin) plugin;
}
}