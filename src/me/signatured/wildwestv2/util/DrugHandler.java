package me.signatured.wildwestv2.util;

import java.io.File;

import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.drugs.DrugSpawn;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class DrugHandler {
	
	private static DrugHandler instance;
	
	public static DrugHandler getInstance() {
		return instance;
	}
	
	private Main plugin;
	private File dfile;
	private YamlConfiguration drugs;
	
	public DrugHandler(Main plugin) {
		instance = this;
		try {
			this.plugin = plugin;
			
			if (!this.plugin.getDataFolder().exists()) {
				this.plugin.getDataFolder().mkdir();
			}
			
			dfile = new File(this.plugin.getDataFolder(), "drugs.yml");
			
			if (dfile.exists()) {
				drugs = YamlConfiguration.loadConfiguration(dfile);
			} else {
				dfile.createNewFile();
				drugs = YamlConfiguration.loadConfiguration(dfile);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void set(String path, Object o) {
		drugs.set(path, o);
		save();
	}
	
	public Object get(String path) {
		return drugs.get(path);
	}
	
	public void save() {
		try {
			drugs.save(dfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getDrugs() {
		return drugs;
	}
	
	public void saveDrugs() {
		for (int i = 0; i < DrugSpawn.drugs.size(); i++) {
			DrugSpawn spawn = DrugSpawn.drugs.get(i);
			
			Location loc = spawn.getLocation();
			Material type = spawn.getType();
			
			set("drugs." + i + ".x", loc.getX());
			set("drugs." + i + ".y", loc.getY());
			set("drugs." + i + ".z", loc.getZ());
			set("drugs." + i + ".world", loc.getWorld().getName());
			set("drugs." + i + ".type", type.name());
			
			save();
			System.out.println("Saved drug type " + i);
		}
	}

}
