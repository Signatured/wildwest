package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class HandcuffAttack implements Listener {
	
	@EventHandler
	public void onHandcuffAttack(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			
			if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
				if (PlayerDataHandler.getInstance().get(player, "handcuffed") == true) {
					e.setCancelled(true);
				}
			}
		}
	}
}
