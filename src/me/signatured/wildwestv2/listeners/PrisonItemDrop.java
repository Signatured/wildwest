package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PrisonItemDrop implements Listener {
	
	@EventHandler
	public void onDropPick(PlayerDropItemEvent e) {
		Player player = e.getPlayer();
		
		if (PlayerDataHandler.getInstance().get(player, "prison") == true) {
			if (e.getItemDrop().getItemStack().getType().equals(Material.DIAMOND_PICKAXE)) {
				e.setCancelled(true);
			}
		}
	}
}
