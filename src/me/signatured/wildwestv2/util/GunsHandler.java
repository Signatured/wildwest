package me.signatured.wildwestv2.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.signatured.wildwestv2.Main;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class GunsHandler {
	
	private static GunsHandler instance;
	
	public static GunsHandler getInstance() {
		return instance;
	}
	
	private Main plugin;
	private File gfile;
	private YamlConfiguration guns;
	
	public GunsHandler(Main plugin) {
		instance = this;
		try {
			this.plugin = plugin;
			
			if (!this.plugin.getDataFolder().exists()) {
				this.plugin.getDataFolder().mkdir();
			}
			
			gfile = new File(this.plugin.getDataFolder(), "guns.yml");
			
			if (gfile.exists()) {
				guns = YamlConfiguration.loadConfiguration(gfile);
			} else {
				gfile.createNewFile();
				guns = YamlConfiguration.loadConfiguration(gfile);
				loadDefaults();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadDefaults() {
		List<String> gunslist = new ArrayList<String>();
    	gunslist.add("IRON_SPADE");
    	gunslist.add("IRON_PICKAXE");
    	
    	if (guns.contains("gunslist") == false) {
    		guns.set("gunslist", gunslist);
    		save();
    	}
	}
	
	public void set(String path, Object o) {
		guns.set(path, o);
		save();
	}
	
	public Object get(String path) {
		return guns.get(path);
	}
	
	public void save() {
		try {
			guns.save(gfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getGuns() {
		return guns;
	}

}
