package me.signatured.wildwestv2.listeners;

import java.util.List;

import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.GunsHandler;
import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class HandcuffGunUse implements Listener {
	
	@EventHandler (priority = EventPriority.HIGHEST)
	public void onGunUseWhileHandcuffed(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		Material item = player.getItemInHand().getType();
		List<String> guns = GunsHandler.getInstance().getGuns().getStringList("gunslist");
		
		if (PlayerDataHandler.getInstance().get(player, "handcuffed") == true) {
			if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (item != null) {
					if (guns.contains(item.name())) {
						e.setCancelled(true);
						player.sendMessage(ChatHandler.getInstance().prefix + "�cYou can't shoot while handcuffed!");
					}
				}
			}
		}
	}
}
