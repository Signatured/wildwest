package me.signatured.wildwestv2.commands;

import me.signatured.wildwestv2.WildWestCommand;
import me.signatured.wildwestv2.util.ChatHandler;
import me.signatured.wildwestv2.util.TownCreatorHandler;

import org.bukkit.entity.Player;

public class SetTown extends WildWestCommand {
	
	@Override
	public String name() {
		return "settown";
	}
	
	@Override
	public String description() {
		return "Set town location";
	}
		
	@Override
	public void run(String[] args, Player sender) {
		if (sender.hasPermission("wildwest.admin")) {
			if (args.length < 0) {
				sender.sendMessage("�c/settown");
				return;
			}
			
			TownCreatorHandler.getInstance().NormalTownCreator(sender.getLocation(), sender.getLocation().getPitch(), sender.getLocation().getYaw());
			int size = TownCreatorHandler.getInstance().towns.size();
			sender.sendMessage("�aTown �c#" + size + " �aset!");
		} else {
			sender.sendMessage(ChatHandler.getInstance().prefix + "�cNot enough permissions!");
		}
	}
}
