package me.signatured.wildwestv2.listeners;

import java.util.List;

import me.mario.bounty.BountyPlayer;
import me.signatured.wildwestv2.Main;
import me.signatured.wildwestv2.spawnselectors.BanditSelector;
import me.signatured.wildwestv2.spawnselectors.SheriffSelector;
import me.signatured.wildwestv2.util.ClothingHandler;
import me.signatured.wildwestv2.util.ConfigHandler;
import me.signatured.wildwestv2.util.JailUtil;
import me.signatured.wildwestv2.util.PlayerDataHandler;
import me.signatured.wildwestv2.util.TabMenuHandler;
import me.signatured.wildwestv2.util.TitleHandler;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class PlayerJoin implements Listener {
	
	public PlayerDataHandler playerdata = PlayerDataHandler.getInstance();
	
	private static PlayerJoin instance;
	
	public PlayerJoin() {
		instance = this;
	}
	
	public static PlayerJoin getInstance() {
		return instance;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		
		TitleHandler.getInstance().sendTitleAndSubtitle(player, "�6WildWest", "�8Howdy Partner!", 10, 60, 10);
		TabMenuHandler.getInstance().sendHeaderAndFooter(player, "�eYou are on �6Brawl.com", "�7Brawl.com");
		
		if (!player.hasPlayedBefore()) {
			Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				public void run() {
					BanditSelector.getInstance().show(player);
				}
			}, 2);
		}
		
		playerdata.createPlayerData(player);
		
		
		if (PlayerDataHandler.getInstance().get(player, "spawnselector") == true) {
			Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				@Override
				public void run() {
					if (e.getPlayer().hasPermission("wildwest.vip") || e.getPlayer().hasPermission("wildwest.mvp")) {
						BanditSelector.getInstance().show(player);
						player.playSound(e.getPlayer().getLocation(), Sound.WITHER_SPAWN, 1, 0);
					}else if (e.getPlayer().hasPermission("wildwest.sheriff")){
						SheriffSelector.getInstance().show(player);
						player.playSound(e.getPlayer().getLocation(), Sound.WITHER_SPAWN, 1, 0);
					} else {
						BanditSelector.getInstance().show(player);
					}
				}
			}, 2);
		}
				
		if (player.getEquipment().getHelmet() == null && player.getEquipment().getChestplate() == null && 
				player.getEquipment().getLeggings() == null && player.getEquipment().getBoots() == null) {
			ClothingHandler.getInstance().giveDefaultGear(player);
		}
		
		if (playerdata.get(player, "knockedout") == true) {
			for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
				pl.hidePlayer(player);
			}
			
			
			Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				@Override
				public void run() {
					for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
						pl.showPlayer(player);
					}
				}
			}, 30L);
		}
		
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
			@Override
			public void run() {				
				if (playerdata.get(player, "knockedout") == true) {
					checkBounty(player);
				}
			}
		}, 30L);
		
		if (playerdata.get(player, "prison") == true) {
			TitleHandler.getInstance().sendTitleAndSubtitleNoFade(player, "�4Welcome to Prison!", "�8Pay off your bounty!");
		}
		
		if (playerdata.get(player, "handcuffed") == true) {
			for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
				pl.hidePlayer(player);
			}
			
			Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				@Override
				public void run() {
					for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
						pl.showPlayer(player);
					}
					checkBounty(player);
				}
			}, 30L);
		}
		
		if (playerdata.get(player, "ticket") == true) {
			TicketClick.sendToJail(player);
		}
		
		if (playerdata.get(player, "dead") == true) {
			for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
				pl.hidePlayer(player);
			}
			
			Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
				@Override
				public void run() {
					for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
						pl.showPlayer(player);
					}
					checkBounty(player);
				}
			}, 30L);
		}
	}
	
	public void checkBounty(Player player) {
		Inventory inv = player.getInventory();
		BountyPlayer bp = BountyPlayer.get(player);
		boolean found = false;
		
		for (ItemStack stack : inv.getContents()) {
			List<String> contraband = ConfigHandler.getInstance().getConfig().getStringList("contraband");
			if (stack != null && contraband.contains(stack.getType().name())) {
				found = true;
				break;
			}
		}
		
		if (found) {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				ItemStack item = inv.getItem(i);
				
				if (item != null) {
					
					switch (item.getType()) {
					
					case IRON_SPADE:
						bp.setBounty(bp.getBounty() + 100);
						inv.clear(i);
						
					default:
						break;
					}
				}
			}
			
			if (bp.getBounty() >= 1) {
				player.setWalkSpeed(0.2F);
				player.removePotionEffect(PotionEffectType.JUMP);
				player.removePotionEffect(PotionEffectType.BLINDNESS);
				
				player.setHealth(20D);
				JailUtil.getInstance().sendToJail(player, inv);
				playerdata.set(player, "knocked out", false);
				playerdata.set(player, "dead", false);
				playerdata.set(player, "handcuffed", false);
				return;
			}
		
		} else {
			if (bp.getBounty() >= 1) {
				player.setWalkSpeed(0.2F);
				player.removePotionEffect(PotionEffectType.JUMP);
				player.removePotionEffect(PotionEffectType.BLINDNESS);
				
				player.setHealth(20D);
				JailUtil.getInstance().sendToJail(player, inv);
				playerdata.set(player, "knocked out", false);
				playerdata.set(player, "dead", false);
				playerdata.set(player, "handcuffed", false);
				return;
			}
			
			player.setHealth(20D);
			player.teleport(player.getWorld().getSpawnLocation());
			player.setWalkSpeed(0.2F);
			
			player.removePotionEffect(PotionEffectType.JUMP);
			player.removePotionEffect(PotionEffectType.BLINDNESS);
			
			player.getInventory().clear();
			ClothingHandler.getInstance().clearArmor(player);
			
			playerdata.set(player, "knocked out", false);
			playerdata.set(player, "dead", false);
			playerdata.set(player, "handcuffed", false);
		
			ClothingHandler.getInstance().giveDefaultGear(player);
			
			BanditSelector.getInstance().show(player);
			playerdata.set(player, "spawnselector", true);
		}
	}
}
