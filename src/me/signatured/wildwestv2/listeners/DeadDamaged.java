package me.signatured.wildwestv2.listeners;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DeadDamaged implements Listener {
	
	@EventHandler (priority = EventPriority.LOWEST)
	public void onDeadDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			
			if (PlayerDataHandler.getInstance().get(player, "dead") == true) {
				e.setCancelled(true);
			}
		}
	}
}
