package me.signatured.wildwestv2.listeners;

import java.util.Arrays;
import java.util.List;

import me.signatured.wildwestv2.util.PlayerDataHandler;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class OreMine implements Listener {
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Player player = e.getPlayer();
		Block block = e.getBlock();
		
		if (PlayerDataHandler.getInstance().get(player, "prison") == false) {
			if (player.getGameMode() == GameMode.SURVIVAL) {
				switch (block.getType()) {
				
				case COAL_ORE:
					if (player.hasPermission("wildwest.mvp")) {
						giveMvpOre(player, block, Material.COAL_ORE, "�eUnprocessed Coal Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else if (player.hasPermission("wildwest.vip")) {
						giveVipOre(player, block, Material.COAL_ORE, "�eUnprocessed Coal Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else {
						giveOre(player, block, Material.COAL_ORE, "�eUnprocessed Coal Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
					}
					break;
				
				case IRON_ORE:
					if (player.hasPermission("wildwest.mvp")) {
						giveMvpOre(player, block, Material.IRON_ORE, "�eUnprocessed Iron Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else if (player.hasPermission("wildwest.vip")) {
						giveVipOre(player, block, Material.IRON_ORE, "�eUnprocessed Iron Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else {
						giveOre(player, block, Material.IRON_ORE, "�eUnprocessed Iron Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
					}
					break;
					
				case GOLD_ORE:
					if (player.hasPermission("wildwest.mvp")) {
						giveMvpOre(player, block, Material.GOLD_ORE, "�eUnprocessed Gold Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else if (player.hasPermission("wildwest.vip")) {
						giveVipOre(player, block, Material.GOLD_ORE, "�eUnprocessed Gold Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else {
						giveOre(player, block, Material.GOLD_ORE, "�eUnprocessed Gold Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
					}
					break;
					
				case DIAMOND_ORE:
					if (player.hasPermission("wildwest.mvp")) {
						giveMvpOre(player, block, Material.DIAMOND_ORE, "�eUnprocessed Diamond Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else if (player.hasPermission("wildwest.vip")) {
						giveVipOre(player, block, Material.DIAMOND_ORE, "�eUnprocessed Diamond Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else {
						giveOre(player, block, Material.DIAMOND_ORE, "�eUnprocessed Diamond Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
					}
					break;
					
				case EMERALD_ORE:
					if (player.hasPermission("wildwest.mvp")) {
						giveMvpOre(player, block, Material.EMERALD_ORE, "�eUnprocessed Emerald Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else if (player.hasPermission("wildwest.vip")) {
						giveVipOre(player, block, Material.EMERALD_ORE, "�eUnprocessed Emerald Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
						return;
					} else {
						giveOre(player, block, Material.EMERALD_ORE, "�eUnprocessed Emerald Ore", Arrays.asList("�cBring to {npc} to process!", " ", "�cx:", "�cz:"));
					}
					break;
					
				default:
					break;
				}
			}
		}
	}
	
	public void giveOre(Player player, Block block, Material material, String displayname, List<String> lore) {
		Inventory pi = player.getInventory();
		
		block.setType(Material.AIR);
		
		ItemStack ore = new ItemStack(material, 1);
		ItemMeta oremeta = ore.getItemMeta();
		
		oremeta.setDisplayName(displayname);
		oremeta.setLore(lore);
		ore.setItemMeta(oremeta);
		pi.addItem(ore);
	}
	
	public void giveVipOre(Player player, Block block, Material material, String displayname, List<String> lore) {
		Inventory pi = player.getInventory();
		
		block.setType(Material.AIR);
		
		ItemStack ore = new ItemStack(material, 2);
		ItemMeta oremeta = ore.getItemMeta();
		
		oremeta.setDisplayName(displayname);
		oremeta.setLore(lore);
		ore.setItemMeta(oremeta);
		pi.addItem(ore);
	}
	
	public void giveMvpOre(Player player, Block block, Material material, String displayname, List<String> lore) {
		Inventory pi = player.getInventory();
		
		block.setType(Material.AIR);
		
		ItemStack ore = new ItemStack(material, 4);
		ItemMeta oremeta = ore.getItemMeta();
		
		oremeta.setDisplayName(displayname);
		oremeta.setLore(lore);
		ore.setItemMeta(oremeta);
		pi.addItem(ore);
	}
}
